﻿using System.Threading.Tasks;

namespace SP.Services.Core.Management.CRM
{
    public interface IZoho
    {
        Task AddAndConvertLead(long userId, long providerId, string username, string msisdn, string name, string location);
    }
}