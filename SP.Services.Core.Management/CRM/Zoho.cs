﻿using Exceptionless;
using Exceptionless.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.Helper;

namespace SP.Services.Core.Management.CRM
{
    public class Zoho : IZoho
    {
        IHelperRepository _helperRepository;
        IApplicationSettings _appSettings;
        ExceptionlessClient _exceptionlessClient = ExceptionlessClientBuilder.ExeptionlessClient;

        public Zoho(IHelperRepository helperRepository, IApplicationSettings applicationSettings)
        {
            _helperRepository = helperRepository;
            _appSettings = applicationSettings;
        }

        public async Task AddAndConvertLead(long userId, long providerId, string username, string msisdn, string name, string location)
        {
            var request = new
            {
                userId = userId,
                providerId = providerId,
                username = username,
                msisdn = msisdn,
                name = name,
                location = location
            };

            string result = await HelperRepository.PostWebData(_appSettings.GrabberUrl, JsonConvert.SerializeObject(request));

            //_exceptionlessClient.SubmitLog($"Post to grabber; request: {_appSettings.GrabberUrl} | data: {JsonConvert.SerializeObject(request)} ______ response: {result}", LogLevel.Debug);
        }
    }
}
