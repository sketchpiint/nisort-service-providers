﻿using System;
using System.Linq;

namespace SP.Services.Core.Management.Extensions
{
    public static class StringExtensions
    {
        public static (int feet, int inches) SplitFeetInches(this string feetInches)
        {
            if (String.IsNullOrEmpty(feetInches)) return (0, 0);

            string[] res = feetInches.Split('\'');

            return (Int32.Parse(res[0]), res.Length > 1 ? Int32.Parse(res[1]) : 0);
        }

        public static bool IsNull(this string _string, bool trueIfZero = false)
        {
            if (trueIfZero) return String.IsNullOrEmpty(_string) || _string.Trim() == "0";
            return String.IsNullOrEmpty(_string);
        }

        /// <summary>
        /// Returns the same (old) string if the <para>newValue</para> is null or empty. Provides '??' functionality for strings
        /// </summary>
        /// <param name="_string">String whose value is changed - returned if newValue is null</param>
        /// <param name="newValue">New string whose value is checked for null or empty</param>
        /// <returns></returns>
        public static string Coalesce(this string _string, string newValue) => String.IsNullOrEmpty(newValue) ? _string : newValue;
    }
}
