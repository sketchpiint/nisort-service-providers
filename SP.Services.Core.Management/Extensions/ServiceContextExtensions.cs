﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;

namespace SP.Services.Core.Management.Extensions
{
    public static class ServiceContextExtensions
    {
        public static void EnsureSeed(this ServiceContext context)
        {
            if (!context.Services.Any())
            {
                context.Services.AddRange(
                    new Service { Name = "Carpentry" },
                    new Service { Name = "Plumbing" },
                    new Service { Name = "Tailoring" },
                    new Service { Name = "Cleaning" },
                    new Service { Name = "House Help Beureau" },
                    new Service { Name = "Interior Design" },
                    new Service { Name = "Interior Fittings" },
                    new Service { Name = "Electrical" },
                    new Service { Name = "Mechanics" },
                    new Service { Name = "Breakdown" },
                    new Service { Name = "Masonry" },
                    new Service { Name = "Software Development" },
                    new Service { Name = "Outside Catering" },
                    new Service { Name = "Photography" },
                    new Service { Name = "Event Organizing" },
                    new Service { Name = "Gardening" },
                    new Service { Name = "Veterinary" },
                    new Service { Name = "Water Delivery" },
                    new Service { Name = "Transport", Tags = "Hand Cart, Pickup, Lorry, Taxi, Bus, Matatu" },
                    new Service { Name = "Electronic Installation" },
                    new Service { Name = "Loading & Unloading", Description = "Get help loading and unloading luggage" },
                    new Service { Name = "Artisan" },
                    new Service { Name = "Painting" },
                    new Service { Name = "House Agency" },
                    new Service { Name = "Brokerage" },
                    new Service { Name = "Food Delivery" },
                    new Service { Name = "car paint" },
                    new Service { Name = "DJ" }
                );
            }

            if (!context.ApplicationSettings.Any())
            {

                context.ApplicationSettings.AddRange(
                        new ApplicationSetting { SettingName = "PasswordLength", SettingValue = "8" },
                        new ApplicationSetting { SettingName = "MinimumWorkFactor", SettingValue = "2" },
                        new ApplicationSetting { SettingName = "SystemEmailSsl", SettingValue = "True" },
                        new ApplicationSetting { SettingName = "SystemEmailPort", SettingValue = "465" },
                        new ApplicationSetting { SettingName = "FusionUserName", SettingValue = "Admin" },
                        new ApplicationSetting { SettingName = "PasswordSaltLength", SettingValue = "256" },
                        new ApplicationSetting { SettingName = "ApplicationName", SettingValue = "Nisort" },
                        new ApplicationSetting { SettingName = "MaximumWorkFactor", SettingValue = "1000" },
                        new ApplicationSetting { SettingName = "SetPasswordExpiryHours", SettingValue = "0" },
                        new ApplicationSetting { SettingName = "InvitationLinkExpiryHours", SettingValue = "0" },
                        new ApplicationSetting { SettingName = "ResetPasswordLinkExpiryHours", SettingValue = "24" },
                        new ApplicationSetting { SettingName = "SystemEmailServer", SettingValue = "smtp.gmail.com" },
                        new ApplicationSetting { SettingName = "SupportMsisdn", SettingValue = "(+254) 714 350 165" },
                        new ApplicationSetting { SettingName = "SupportEmail", SettingValue = "sketchpiint@gmail.com" },
                        new ApplicationSetting { SettingName = "FusionPassword", SettingValue = "Let Me In! Please...0192" },
                        new ApplicationSetting { SettingName = "SystemEmailSender", SettingValue = "sketchpiint@gmail.com" },
                        new ApplicationSetting { SettingName = "SystemEmailPassword", SettingValue = "Let Me In! Please...0192" },
                        new ApplicationSetting { SettingName = "SetPasswordLink", SettingValue = "http://52.178.110.185/set-password" },
                        new ApplicationSetting { SettingName = "QueryStringToken", SettingValue = "c4d58bed-b06f-4e35-8e03-90d5e0327c23" },
                        new ApplicationSetting { SettingName = "ResetPasswordLink", SettingValue = "http://52.178.110.185/reset-password" },
                        new ApplicationSetting { SettingName = "FusionApiUrl", SettingValue = "http://178.79.129.150:8764/api/apollo/query-pipelines/MicroServices-default/collections/MicroServices/select" },
                        new ApplicationSetting { SettingName = "ImagesLocation", SettingValue = "{hostingAbsolutePath}/albums/{userId}/" },
                        new ApplicationSetting { SettingName = "MaximumImageCount", SettingValue = "5" },
                        new ApplicationSetting { SettingName = "GracePeriodDays", SettingValue = "30" },
                        new ApplicationSetting { SettingName = "SmsApiUrl", SettingValue = "http://api.valdisms.co.ke/api/Sms/" },
                        new ApplicationSetting { SettingName = "SmsApiUsername", SettingValue = "646C6E3FC34F4B2B93378316CB91B824" },
                        new ApplicationSetting { SettingName = "SmsApiPassword", SettingValue = "E58B46AF45D843489DBC4AD281AD6E9D" },
                        new ApplicationSetting { SettingName = "SmsApiSenderId", SettingValue = "NairobiDivas" },
                        new ApplicationSetting { SettingName = "TinyPngKeys", SettingValue = JsonConvert.SerializeObject(new Dictionary<string, bool> { { "CAMdtwwUNQnFRQf_EEel92kGaxYQ0VV8", true }, { "uKklDYy888uiE9rF_UAua4gJluAdxh7I", false } })},
                        new ApplicationSetting { SettingName = "GrabberUrl", SettingValue = "40.68.191.145/grabber/api/" }
                     );
                

                
            }

            if (!context.SecurityLinkTokenType.Any())
            {

                #region SecurityLinkTokenTypes
                //Does not set the right order in pgsql
                //Enum.GetNames(typeof(SecurityLinkTokenTypes)).ToList().ForEach(ett =>
                //{
                //    context.SecurityLinkTokenType.Add(new SecurityLinkTokenType { Name = ett });
                //});

                context.SecurityLinkTokenType.AddRange(
                    new SecurityLinkTokenType { Name = SecurityLinkTokenTypes.SetPassword.ToString() },
                    new SecurityLinkTokenType { Name = SecurityLinkTokenTypes.ResetPassword.ToString() },
                    new SecurityLinkTokenType { Name = SecurityLinkTokenTypes.Invitation.ToString() });
                #endregion

            }

            if (!context.SuspensionReasons.Any())
            {
                context.SuspensionReasons.AddRange(
                    new SuspensionReason { Name = SuspensionReasons.LowAccountBalance.ToString() },
                    new SuspensionReason { Name = SuspensionReasons.Administrative.ToString() });
            }

            if (!context.NotificationTypes.Any())
            {
                context.NotificationTypes.AddRange(
                    new NotificationType { Name = NotificationTypes.Email.ToString() },
                    new NotificationType { Name = NotificationTypes.Sms.ToString() },
                    new NotificationType { Name = NotificationTypes.All.ToString() });
            }

            context.SaveChanges();
        }
    }
}
