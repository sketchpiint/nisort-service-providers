﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SP.Services.Core.Management.Logging
{
    public class CustomException : Exception
    {
        public CustomException(Exception exception, string technicalMessage, string friendlyMessage, string errorCode)
        {
            HttpStatusCode = HttpStatusCode.OK;

            Exception = exception;
            TechnicalMessage = technicalMessage;
            FriendlyMessage = friendlyMessage;
            F3ErrorCode = errorCode;
        }

        public CustomException(Exception exception, string technicalMessage, string friendlyMessage, HttpStatusCode errorCode)
        {
            Exception = exception;
            TechnicalMessage = technicalMessage;
            FriendlyMessage = friendlyMessage;
            F3ErrorCode = $"F3-{(int)errorCode}";
            HttpStatusCode = errorCode;
        }

        public CustomException(string technicalMessage, string friendlyMessage, string errorCode)
        {
            HttpStatusCode = HttpStatusCode.OK;

            Exception = new Exception(technicalMessage);
            TechnicalMessage = technicalMessage;
            FriendlyMessage = friendlyMessage;
            F3ErrorCode = errorCode;
        }

        public CustomException(string technicalMessage, string friendlyMessage, HttpStatusCode errorCode)
        {
            Exception = new Exception(technicalMessage);
            TechnicalMessage = technicalMessage;
            FriendlyMessage = friendlyMessage;
            F3ErrorCode = $"F3-{(int)errorCode}";
            HttpStatusCode = errorCode;
        }

        public CustomException(string friendlyMessage, string errorCode)
        {
            HttpStatusCode = HttpStatusCode.OK;

            Exception = new Exception(friendlyMessage);
            TechnicalMessage = friendlyMessage;
            FriendlyMessage = friendlyMessage;
            F3ErrorCode = errorCode;
        }

        public CustomException(string friendlyMessage, HttpStatusCode errorCode)
        {
            Exception = new Exception(friendlyMessage);
            TechnicalMessage = friendlyMessage;
            FriendlyMessage = friendlyMessage;
            F3ErrorCode = $"F3-{(int)errorCode}";
            HttpStatusCode = errorCode;
        }

        public Exception Exception { get; set; }
        public string FriendlyMessage { get; set; }
        public string TechnicalMessage { get; set; }
        public string F3ErrorCode { get; set; }

        /// <summary>
        /// If set to HttpStatusCode.OK then no http compatible error was found. Return something generic like Bad Request
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
    }
}
