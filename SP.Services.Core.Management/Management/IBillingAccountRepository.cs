﻿using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.RepositoryBases;

namespace SP.Services.Core.Management
{
    public interface IBillingAccountRepository : IObjectRepository<BillingAccount>
    {
        ServiceContext Context { get; }

        Task<long> PreloadAccount(int providerId, int amount);
        Task SuspendUnpaidAccounts();
        Task PauseProviderBillingCycle(long providerId, int intervalDays = 0, string pausedBy = "System - Initial Create");
        Task PauseBillingCycle(long billingAccountId, int intervalDays = 0, string pausedBy = "System - Initial Create");
        Task PauseBillingCycle(BillingAccount account, int intervalDays = 0, string pausedBy = "System - Initial Create");
        int GetDaysToNextPayment(long providerId);
    }
}