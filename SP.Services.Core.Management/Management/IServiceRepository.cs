﻿using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.RepositoryBases;

namespace SP.Services.Core.Management
{
    public interface IServiceRepository:IObjectRepository<Service>
    {
        ServiceContext Context { get; }
    }
}