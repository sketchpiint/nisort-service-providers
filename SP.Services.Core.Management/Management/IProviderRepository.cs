﻿using System.Threading.Tasks;
using System.Collections.Generic;

using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.RepositoryBases;
using System;

namespace SP.Services.Core.Management
{
    public interface IProviderRepository : IObjectRepository<Provider>
    {
        ServiceContext Context { get; }

        Task AddProviderService(List<ProviderService> providerServices);
        Task AddProviderService(long providerId, int serviceId);
        void BulkInsertProviders(List<Provider> providers);
        Task<Provider> CreateProvider(long userId, string boundingBoxJson, string generalLocation, MobilityTypes mobilityType, ProviderTypes providerType, string chestCentimeters, string heightCentimeters, DateTime dateOfBirth, string eyeColor, string hairColor, string hairType, string nationality, string physique, string waistCentimeters, string languages, string area, string town, bool suspended, string bio, string availability, string metaDataJson, int weight);
        Task<List<ProviderService>> GetProviderServices(long providerId);
        Task<List<Service>> GetServices(long providerId);
        Task RemoveProviderService(long providerId, int serviceId);
        Task<(int TotalCount, List<Provider> Providers)> GetProviders(int serviceId, int startIndex, int pageSize, string orderBy = "ProviderId DESC");
        Task<Provider> EditProvider(Provider newDetails);
        Task<(List<ProviderService> services, int count)> GetProviderServices(int pageSize, int startIndex = 0, string orderByClause = "ProviderServiceId DESC");
        Task<(List<(Provider provider, List<Service> services)> providerServices, int count)> GetProvidersWithServices(int pageSize, int startIndex = 0, string orderByClause = "ProviderServiceId DESC");
        Task<(List<Provider> data, int totalCount)> GetProviders(string area, int pageSize, int startIndex = 0, string orderByClause = "ProviderId DESC");
        Task<(List<Provider> data, int totalCount)> GetProvidersByTown(string town, int pageSize, int startIndex = 0, string orderByClause = "ProviderId DESC");
        List<string> GetRegisteredAreas();
        List<string> GetRegisteredTowns();
        //(int TotalCount, List<Provider> Providers) Filter(List<string> locations, int minimumAge, int maximumAge, string name, List<int> serviceIds, int minimumHeightCm, int maximumHeightCm, int pageSize, int startIndex, string orderByClause, bool administration = false);

        (int TotalCount, List<Provider> Providers) Filter(List<string> locations, int minimumAge, int maximumAge, string name, List<int> serviceIds, int minimumHeightCm, int maximumHeightCm, int pageSize, int startIndex, string orderByClause, DateTime? addedAfter = null, DateTime? addedBefore = null, string username = "", bool? suspended = false, bool admin = false);

        Task Reinstate(long providerId, string reinstatedBy, string reinstateDetail);
        Task Suspend(long providerId, SuspensionReasons reason, string detail, string suspendedBy);
        Task<List<Provider>> GetWithNonExplicitImages(int count);
    }
}