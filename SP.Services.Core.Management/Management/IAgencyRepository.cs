﻿using System.Threading.Tasks;

using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.RepositoryBases;

public interface IAgencyRepository : IObjectRepository<Agency>
{
    ServiceContext Context { get; }

    Task<Agency> CreateAgency(long agencyId, string name, string email, string website, string country, string bio, string boundingBoxJson, string generalLocation);
    Task<Agency> EditAgency(Agency newDetails);
}