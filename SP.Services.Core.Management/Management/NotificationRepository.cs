﻿//using LinqKit;
//using System.Linq.Dynamic;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.Communication;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.DTOs;
using SP.Services.Core.Management.Extensions;
using SP.Services.Core.Management.RepositoryBases;

namespace SP.Services.Core.Management
{
    public class NotificationRepository : EFCoreObjectRepository<Notification>, INotificationRepository
    {
        IUserRepository _userRepository;
        IEmail _email;
        ISms _sms;
        IApplicationSettings _appSettings;

        public NotificationRepository(ServiceContext context, IUserRepository userRepository, IEmail email, ISms sms, IApplicationSettings applicationSettings) : base(context)
        {
            _userRepository = userRepository;
            _email = email;
            _sms = sms;
            _appSettings = applicationSettings;
        }

        public ServiceContext Context { get { return (ServiceContext)_context; } }

        public async Task<bool> QueueNotification(long userId, NotificationTypes type, string message, string subject, string msisdn = "", string email = "")
        {
            bool result = false;

            User user = _userRepository.ValidatedSearch(u => u.UserId == userId);

            Notification notification = new Notification
            {
                Message = message,
                NotificationTypeId = (int)type,
                DateQueued = DateTime.Now,
                UserId = userId,
                Subject = subject,
                Msisdn = msisdn.IsNull() ? user.Msisdn : msisdn,
                Email = email.IsNull() ? user.Email : email
            };

            Add(notification);
            await SaveAsync();
            result = true;

            return result;
        }

        public async Task<bool> SendNotifications()
        {
            bool result = false;

            var notifications = Search(n => !n.Sent && n.Retry).ToList();

            notifications.ForEach(async n =>
            {
                try
                {
                    if (n.NotificationTypeId == (int)NotificationTypes.Email || n.NotificationTypeId == (int)NotificationTypes.All)
                    {
                        await _email.Send(new List<string> { n.Email }, n.Subject, n.Message);
                    }

                    if (n.NotificationTypeId == (int)NotificationTypes.Sms || n.NotificationTypeId == (int)NotificationTypes.All)
                    {
                        await _sms.Send(new SmsPayload
                        {
                            Message = n.Message,
                            phoneNumber = n.Msisdn,
                            Recipients = new List<SmsRecipient> {
                                new SmsRecipient {
                                    Msisdn = n.Msisdn,
                                    Third_party_message_id = n.NotificationId.ToString()
                                } },
                            Sender = _appSettings.SmsApiSenderId
                        });
                    }

                    n.Sent = true;
                }
                catch (Exception ex)
                {
                    n.Sent = false;
                    n.Retry = ++n.RetryCount <= 6;
                    n.Error = ex.ToString();
                }
            });

            await SaveAsync();

            result = true;
            return result;
        }
    }
}