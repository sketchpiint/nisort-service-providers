﻿using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.RepositoryBases;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SP.Services.Core.Engine.Database;

namespace SP.Services.Core.Management
{
    public class ServiceRepository : EFCoreObjectRepository<Service>, IServiceRepository
    {
        public ServiceRepository(ServiceContext context) : base(context)
        {
        }

        public ServiceContext Context { get { return (ServiceContext)_context; } }        
    }
}
