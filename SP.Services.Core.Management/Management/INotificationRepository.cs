﻿using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.RepositoryBases;

namespace SP.Services.Core.Management
{
    public interface INotificationRepository:IObjectRepository<Notification>
    {
        ServiceContext Context { get; }

        Task<bool> QueueNotification(long userId, NotificationTypes type, string message, string subject, string msisdn = "", string email = "");
        Task<bool> SendNotifications();
    }
}