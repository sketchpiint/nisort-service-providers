﻿//using LinqKit;
//using System.Linq.Dynamic;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.Helper;
using SP.Services.Core.Management.RepositoryBases;

namespace SP.Services.Core.Management
{
    public class BillingAccountRepository : EFCoreObjectRepository<BillingAccount>, IBillingAccountRepository
    {
        IUserRepository _userRepository;
        IApplicationSettings _appSettings;
        IProviderRepository _providerRepository;

        public BillingAccountRepository(ServiceContext context, IApplicationSettings applicationSettings, IProviderRepository providerRepository, IUserRepository userRepository) : base(context)
        {
            _appSettings = applicationSettings;
            _providerRepository = providerRepository;
            _userRepository = userRepository;
        }

        public ServiceContext Context { get { return (ServiceContext)_context; } }

        public async Task<long> PreloadAccount(int providerId, int amount)
        {
            DateTime transactionTime = DateTime.Now;

            Provider provider = _providerRepository.ValidatedSearch(p => p.ProviderId == providerId);
            BillingAccount account = ValidatedSearch(b => b.ProviderId == provider.ProviderId);

            TransactionLog log = new TransactionLog
            {
                Amount = amount,
                BalanceBefore = account.AccountBalance,
                BalanceAfter = account.AccountBalance + amount,
                ProviderId = provider.ProviderId,
                TransactionDate = transactionTime,
                BillingAccountId = account.BillingAccountId,
                ExtraDetail = ""
            };

            await Context.TransactionLogs.AddAsync(log);

            account.AccountBalance += amount;

            await SaveAsync();

            if (provider.Suspended) await _providerRepository.Reinstate(providerId, "Billing System", "Account reloaded");

            User user = _userRepository.ValidatedSearch(u => u.UserId == provider.UserId);

            Notification notification = new Notification
            {
                DateQueued = DateTime.Now,
                Message = $"Dear {user.FirstName}; Your payment of {amount.ToString("C").Replace("$", "KSh.")} has successfully been received for Nairobi Divas. Thank you for using our services.",
                NotificationTypeId = (int)NotificationTypes.Sms,
                UserId = provider.UserId,
                Msisdn = user.Msisdn
            };

            await Context.Notifications.AddAsync(notification);

            await SaveAsync();

            return account.AccountBalance;
        }

        public async Task SuspendUnpaidAccounts()
        {
            DateTime now = DateTime.Now;
            List<BillingAccount> accounts = Search(a => a.NextPaymentDate <= now && !a.BillingTimerPaused).ToList();

            using (ServiceContext serviceContext = new ServiceContext(Context.ContextOptions))
            {
                accounts.ForEach(a =>
                {
                    Provider provider = new Provider { ProviderId = a.ProviderId };
                    serviceContext.Providers.Attach(provider);

                    provider.Suspended = true;
                });

                await serviceContext.SaveChangesAsync();
            }
        }

        public async Task PauseProviderBillingCycle(long providerId, int intervalDays = 0, string pausedBy = "System - Initial Create")
        {
            BillingAccount account = ValidatedSearch(b => b.ProviderId == providerId);
            await PauseBillingCycle(account, intervalDays, pausedBy);
        }

        public async Task PauseBillingCycle(long billingAccountId, int intervalDays = 0, string pausedBy = "System - Initial Create")
        {
            BillingAccount account = ValidatedSearch(a => a.BillingAccountId == billingAccountId);

            await PauseBillingCycle(account, intervalDays, pausedBy);
        }

        public async Task PauseBillingCycle(BillingAccount account, int intervalDays = 0, string pausedBy = "System - Initial Create")
        {
            DateTime now = DateTime.Now;

            int interval = intervalDays == 0 ? _appSettings.GracePeriodDays : intervalDays;

            account.DatePaused = now;
            account.DaysToCountDown = interval;
            account.PausedBy = pausedBy;
            account.NextPaymentDate = now.AddDays(interval);

            await SaveAsync();
        }

        public int GetDaysToNextPayment(long providerId)
        {
            BillingAccount account = ValidatedSearch(p => p.ProviderId == providerId);

            return (int)account.NextPaymentDate.Subtract(DateTime.Now).TotalDays;
        }
    }
}