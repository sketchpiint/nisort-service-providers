﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.RepositoryBases;

namespace SP.Services.Core.Management
{
    public interface IUserRepository : IObjectRepository<User>
    {
        ServiceContext Context { get; }

        Task<(User user, string token)> CreateUserAsync(string firstName, string lastName, string otherNames, string msisdn, string username, string email);
        Task<string> SendSetPasswordLink(long userId);
        Task<bool> SetUserPasswordAsync(string token, string password, bool reset = false);
        Task<User> UserLogin(string identifier, string passwordClaim);
        Task<bool> ValidateSecurityToken(string linkToken, SecurityLinkTokenTypes type);
        Task<(bool success, string token)> RecoverPasswordAsync(string userIdentity, bool administration = false);
        User GetUserByIdentifier(string identifier, Exception exception = null);
        Task<User> EditUser(User newDetails);
        Task<bool> AddImage(byte[] fileContent, string fileName, string contentType, bool isProfilePicture, long userId, string hostingAbsolutePath);
        List<string> GetUserImageNames(User user, bool? safeOnly = null);
        List<string> GetUserImageNames(long userId, bool? safeOnly = null);
        bool DeleteUserImage(long userId, string imageName);
        void MarkImageExplicit(long userId, string imageName);
        void MarkImageSafe(long userId, string imageName);
        bool EditImageName(long userId, string oldImageName, string newImageName);
        Task SendFeedback(string email, string fullName, string msisdn, string message);
        Task UpdateDateCreated();
    }
}