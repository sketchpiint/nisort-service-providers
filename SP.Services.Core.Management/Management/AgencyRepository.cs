﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.Extensions;
using SP.Services.Core.Management.Helper;
using SP.Services.Core.Management.RepositoryBases;

public class AgencyRepository : EFCoreObjectRepository<Agency>, IAgencyRepository
{
    IHelperRepository _helperRepository;
    public AgencyRepository(ServiceContext context, IHelperRepository helperRepository) : base(context)
    {
        _helperRepository = helperRepository;
    }

    public ServiceContext Context { get { return (ServiceContext)_context; } }

    public async Task<Agency> CreateAgency(long agencyId, string name, string email, string website, string country, string bio, string boundingBoxJson, string generalLocation)
    {
        string[] locationInfo = generalLocation.Split(',');
        string town = "", area = "";
        if (locationInfo.Any())
        {
            area = locationInfo[0];
            town = locationInfo[1];
        }

        Agency agency = new Agency
        {
            Active = true,
            Name = name,
            Email = email,
            Website = website,
            Country = country,
            Bio = bio,
            BoundingBoxJson = boundingBoxJson,
            GeneralLocation = generalLocation,
            Town = town,
            Area = area,
        };

        await Context.Agencies.AddAsync(agency);
        await SaveAsync();

        return agency;
    }

    public async Task<Agency> EditAgency(Agency newDetails)
    {
        long agencyId = newDetails.AgencyId;
        Agency agency = ValidatedSearch(p => p.AgencyId == agencyId);

        string[] locationInfo = newDetails.GeneralLocation.Split(',');
        string town = "", area = "";
        if (locationInfo.Any())
        {
            area = locationInfo[0];
            town = locationInfo[1];
        }

        agency.Town = agency.Town.Coalesce(town);
        agency.Area = agency.Area.Coalesce(area);
        agency.Country = agency.Country.Coalesce(newDetails.Country);
        agency.BoundingBoxJson = agency.BoundingBoxJson.Coalesce(newDetails.BoundingBoxJson);
        agency.GeneralLocation = agency.GeneralLocation.Coalesce(newDetails.GeneralLocation);
        agency.Name = agency.Name.Coalesce(newDetails.Name);
        agency.Email = agency.Email.Coalesce(newDetails.Email);
        agency.Website = agency.Website.Coalesce(newDetails.Website);
        agency.Bio = agency.Bio.Coalesce(newDetails.Bio);

        await SaveAsync();

        return agency;
    }

    public async Task<bool> SetAgencyPasswordAsync(string token, string password, bool reset = false)
    {

        _helperRepository.EvaluatePasswordSecurity(password);

        SecurityLinkToken linkToken = await _helperRepository.RetrieveSecurityToken(reset ? SecurityLinkTokenTypes.ResetPassword : SecurityLinkTokenTypes.SetPassword, token);

        string email = linkToken.Email;

        Agency agency = ValidatedSearch(u => u.Email == email);

        return await SetAgencyPasswordAsync(agency.Email, password, agency);
    }

    private async Task<bool> SetAgencyPasswordAsync(string email, string password, Agency existingAgency = null)
    {

        _helperRepository.EvaluatePasswordSecurity(password);

        Agency agency = existingAgency ?? ValidatedSearch(a => a.Email == email, checkActive: false, throwException: true);

        string salt = "";
        string hash = "";

        HelperRepository.SecurePassword(password, agency.WorkFactor, out salt, out hash);

        agency.Salt = Encoding.ASCII.GetBytes(salt);
        agency.Password = Encoding.ASCII.GetBytes(hash);
        agency.PasswordSet = true;

        await SaveAsync();

        return true;
    }

    //public async Task<User> UserLogin(string identifier, string passwordClaim)
    //{
    //    CustomException exception = new CustomException("Invalid username or password", HttpStatusCode.Unauthorized);

    //    try
    //    {
    //        User user = GetUserByIdentifier(identifier, exception);

    //        string salt = Encoding.ASCII.GetString(user.Salt);

    //        string hashClaim = HelperRepository.SecurePassword(passwordClaim, salt, user.WorkFactor);

    //        if (hashClaim.Equals(Encoding.ASCII.GetString(user.Password)))
    //        {
    //            user.WorkFactor = HelperRepository.GetNewWorkFactor(user.WorkFactor);

    //            await SetUserPasswordAsync(user.Username, passwordClaim, user);

    //            return user;
    //        }

    //        throw exception;
    //    }
    //    catch (System.Exception ex)
    //    {
    //        throw exception;
    //    }

    //}

    //public async Task<bool> RecoverPasswordAsync(string userIdentity)
    //{
    //    User user = GetUserByIdentifier(userIdentity);

    //    await _helperRepository.RequestSecurityLinkTokenAsync(SecurityLinkTokenTypes.ResetPassword, user.Email);
    //    return true;
    //}
}