﻿using System;
using System.IO;
using TinifyAPI;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.Helper;
using SP.Services.Core.Management.Logging;
using SP.Services.Core.Management.Communication;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.RepositoryBases;

using kvp = System.Collections.Generic.KeyValuePair<string, string>;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.Extensions;
using System.Text.RegularExpressions;
using ImageMagick;

namespace SP.Services.Core.Management
{

    public class UserRepository : EFCoreObjectRepository<User>, IUserRepository
    {
        IServiceRepository _serviceRepository;
        IEmail _email;
        IApplicationSettings _appSettings;
        IHelperRepository _helperRepository;

        public UserRepository(ServiceContext context, IServiceRepository serviceRepository, IEmail email, IApplicationSettings appSettings, IHelperRepository helperRepository) : base(context)
        {
            _email = email;
            _appSettings = appSettings;
            _helperRepository = helperRepository;
            _serviceRepository = serviceRepository;
        }

        public ServiceContext Context { get { return (ServiceContext)_context; } }

        public async Task<(User user, string token)> CreateUserAsync(string firstName, string lastName, string otherNames, string msisdn, string username, string email)
        {
            msisdn = msisdn.Trim();
            email = email.Trim();
            username = username.Trim().ToLower();

            HelperRepository.ValidateEmailAddress(email);

            ValidatedSearch(u => u.Msisdn == msisdn, inverseCheck: true);
            ValidatedSearch(u => u.Email == email, inverseCheck: true);
            User user = ValidatedSearch(u => u.Username == username, inverseCheck: true);

            user = new User
            {
                Active = true,
                Msisdn = msisdn,
                IsProvider = false,
                PasswordSet = false,
                Email = email.ToLower(),
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
                Username = username.ToLower(),
                LastName = HelperRepository.SentenceCase(lastName),
                FirstName = HelperRepository.SentenceCase(firstName),
                WorkFactor = HelperRepository.GetNewWorkFactor(0, newUser: true),
                OtherName = HelperRepository.SentenceCase(otherNames.Coalesce(" ")),
            };

            Add(user);

            await SaveAsync();

            string token = await SendSetPasswordLink(user.UserId);

            return (user, token);
        }

        public async Task<User> EditUser(User newDetails)
        {
            long userId = newDetails.UserId;
            User user = ValidatedSearch(p => p.UserId == userId);

            user.FirstName = String.IsNullOrEmpty(newDetails.FirstName) ? user.FirstName : newDetails.FirstName;
            user.LastName = String.IsNullOrEmpty(newDetails.LastName) ? user.LastName : newDetails.LastName;
            user.Msisdn = String.IsNullOrEmpty(newDetails.Msisdn) ? user.Msisdn : newDetails.Msisdn;
            user.Email = String.IsNullOrEmpty(newDetails.Email) ? user.Email : newDetails.Email;
            user.OtherName = String.IsNullOrEmpty(newDetails.OtherName) ? user.OtherName : newDetails.OtherName;
            user.Username = String.IsNullOrEmpty(newDetails.Username) ? user.Username : newDetails.Username;
            user.IsProvider = newDetails.IsProvider;
            user.DateModified = DateTime.Now;

            await SaveAsync();

            return user;
        }

        public async Task<string> SendSetPasswordLink(long userId)
        {
            User user = ValidatedSearch(p => p.UserId == userId);

            Guid linkToken = await _helperRepository.RequestSecurityLinkTokenAsync(SecurityLinkTokenTypes.SetPassword, user.Email);

            if (user.Email.Contains($"{user.Msisdn}@nairobidivas.com")) return linkToken.ToString();

            string link = $"{_appSettings.SetPasswordLink}?{_appSettings.QueryStringToken}={linkToken}";

            string body = await _email.GetEmailBody("SP.Services.Core.Management.set_password.html",
                new kvp("{Msisdn}", user.Msisdn),
                new kvp("{UserEmail}", user.Email),
                new kvp("{Username}", user.Username),
                new kvp("{FirstName}", user.FirstName),
                new kvp("{ActivateAccountLink}", link),
                new kvp("{CarePhone}", _appSettings.SupportMsisdn),
                new kvp("{CareEmail}", _appSettings.SupportEmail));

            await _email.Send(new List<string> { user.Email }, $"Welcome to {_appSettings.ApplicationName}", body);

            return linkToken.ToString();
        }

        public async Task<bool> ValidateSecurityToken(string linkToken, SecurityLinkTokenTypes type) => await _helperRepository.RetrieveSecurityToken(type, linkToken, validate: true) != null;

        public async Task<bool> SetUserPasswordAsync(string token, string password, bool reset = false)
        {

            _helperRepository.EvaluatePasswordSecurity(password);

            SecurityLinkToken linkToken = await _helperRepository.RetrieveSecurityToken(reset ? SecurityLinkTokenTypes.ResetPassword : SecurityLinkTokenTypes.SetPassword, token);

            string email = linkToken.Email;

            User user = ValidatedSearch(u => u.Email == email);

            return await SetUserPasswordAsync(user.Username, password, user);
        }

        private async Task<bool> SetUserPasswordAsync(string username, string password, User existingUser = null)
        {

            _helperRepository.EvaluatePasswordSecurity(password);

            User user = existingUser ?? ValidatedSearch(u => u.Username == username, checkActive: false, throwException: true);

            string salt = "";
            string hash = "";

            HelperRepository.SecurePassword(password, user.WorkFactor, out salt, out hash);

            user.Salt = Encoding.ASCII.GetBytes(salt);
            user.Password = Encoding.ASCII.GetBytes(hash);
            user.PasswordSet = true;

            await SaveAsync();

            return true;
        }

        public async Task<User> UserLogin(string identifier, string passwordClaim)
        {
            CustomException exception = new CustomException("Invalid username or password", HttpStatusCode.Unauthorized);

            try
            {
                User user = GetUserByIdentifier(identifier, exception);

                string salt = Encoding.ASCII.GetString(user.Salt);

                string hashClaim = HelperRepository.SecurePassword(passwordClaim, salt, user.WorkFactor);

                if (hashClaim.Equals(Encoding.ASCII.GetString(user.Password)))
                {
                    user.WorkFactor = HelperRepository.GetNewWorkFactor(user.WorkFactor);

                    await SetUserPasswordAsync(user.Username, passwordClaim, user);

                    return user;
                }

                throw exception;
            }
            catch (System.Exception ex)
            {
                throw exception;
            }

        }

        public async Task<(bool success, string token)> RecoverPasswordAsync(string userIdentity, bool administration = false)
        {
            User user = GetUserByIdentifier(userIdentity);

            var tokenGuid = await _helperRepository.RequestSecurityLinkTokenAsync(SecurityLinkTokenTypes.ResetPassword, user.Email);
            return (true, administration ? tokenGuid.ToString() : "");
        }

        public User GetUserByIdentifier1(string identifier, System.Exception exception = null)
        {
            bool throwInbuiltException = exception == null;
            User user = null;

            if (Regex.IsMatch(identifier, @"^(?:254|\+254|0)?(7(?:(?:[12][0-9])|(?:0[0-8])|(?:9[0-2]))[0-9]{6})$"))
            {
                user = ValidatedSearch(u => u.Msisdn == identifier, throwException: throwInbuiltException);
                if (user == null && exception != null) throw exception;
                return user;
            }

            if (HelperRepository.ValidateEmailAddress(identifier, throwException: throwInbuiltException))
            {
                user = ValidatedSearch(u => u.Email == identifier, checkActive: true, throwException: throwInbuiltException);

                if (user == null && exception != null) throw exception;
                return user;
            }

            user = ValidatedSearch(u => u.Username == identifier, checkActive: true, throwException: throwInbuiltException);

            if (user == null && exception != null) throw exception;

            return user;
        }

        public User GetUserByIdentifier(string identifier, System.Exception exception = null)
        {
            bool throwInbuiltException = exception == null;
            User user = null;
            string id = identifier.ToLower();

            user = ValidatedSearch(u => u.Username == id, checkActive: true, throwException: false);
            if (user != null) return user;

            user = ValidatedSearch(u => u.Email == id, checkActive: true, throwException: false);
            if (user != null) return user;

            user = ValidatedSearch(u => u.Msisdn == id, checkActive: true, throwException: false);
            if (user != null) return user;

            if (exception != null) throw exception;

            throw new CustomException($"User account with the identifier {identifier} could not be found", HttpStatusCode.NotFound);
        }

        public async Task<bool> AddImage(byte[] fileContent, string fileName, string contentType, bool isProfilePicture, long userId, string hostingAbsolutePath)
        {
            User user = ValidatedSearch(u => u.UserId == userId);

            string imageExtension = GetImageFormat(fileContent);
            string imagesLocation = _appSettings.ImagesLocation
                .Replace("{hostingAbsolutePath}", hostingAbsolutePath)
                .Replace("{userId}", userId.ToString());

            string watermarkLocation = $"{imagesLocation.Replace($"{userId}/", "")}/ndivas-watermark-stronger.png";

            if (!Directory.Exists($"{imagesLocation}\\Original")) Directory.CreateDirectory($"{imagesLocation}\\Original");

            if (Directory.EnumerateFiles(imagesLocation, $"*{imageExtension}").Where(fn => !fn.EndsWith($"profile.{imageExtension}")).Count() >= _appSettings.MaximumImageCount && !isProfilePicture)
                throw new CustomException($"You can only upload a maximum of {_appSettings.MaximumImageCount} images", HttpStatusCode.NotAcceptable);

            //string imageName = isProfilePicture ? $"{user.UserId}-profile" : $"{user.FirstName}_{_appSettings.ApplicationName}_{DateTime.Now.ToString("ddMMyyHHmmss")}_{user.UserId}";

            string imageName = $"{user.FirstName}-{_appSettings.ApplicationName}-{DateTime.Now.ToString("ddMMyyHHmmss")}-{user.UserId}";

            if (isProfilePicture)
            {
                var profilePics = Directory.EnumerateFiles(imagesLocation, "*profile.*");
                profilePics.ToList().ForEach(pp => File.Delete(pp));
            }

            imageName += $".{imageExtension}";

            File.WriteAllBytes($"{imagesLocation}\\Original\\{imageName}", fileContent);

            user.ProfilePictureUrl = imagesLocation;
            user.AlbumUrl = imagesLocation;

            await SaveAsync();

            await ProcessImage(fileContent, imagesLocation, imageName, watermarkLocation);

            return true;
        }

        private async Task ProcessImage(byte[] imageBuffer, string imagesLocation, string imageName, string watermarkLocation)
        {
            var keys = _appSettings.TinyPngKeys;

            try
            {
                if (keys == null || !keys.Any(k => k.Value))
                    throw new CustomException($"There's a problem with the configuration of the TinyPNG API Key. Current value is {keys}. There should always be at least one key enabled. Only the forst key will be used when multiple are enabled.", "AN internal error occurred uploading the image. Please raise this issue with support", HttpStatusCode.InternalServerError);

                Tinify.Key = keys.FirstOrDefault(k => k.Value).Key; //"CAMdtwwUNQnFRQf_EEel92kGaxYQ0VV8";

                var compressed = await Tinify.FromBuffer(imageBuffer).Resize(new
                {
                    method = "fit",
                    width = 500,
                    height = 500
                });

                //using (MagickImage image = new MagickImage(await compressed.ToBuffer()))
                //{
                //    using (MagickImage watermark = new MagickImage(watermarkLocation))
                //    {
                //        image.Composite(watermark, Gravity.West, CompositeOperator.Overlay);

                //        // Optionally make the watermark more transparent
                //        watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);

                //    }

                //    image.Write($"{imagesLocation}/{imageName}");
                //}

                await compressed.ToFile($"{imagesLocation}/{imageName}");

                if (!Directory.Exists($"{imagesLocation}\\Thumbnails")) Directory.CreateDirectory($"{imagesLocation}\\Thumbnails");

                using (MagickImage image = new MagickImage(imageBuffer))
                {
                    MagickGeometry size = new MagickGeometry(100);

                    // This will resize the image to a fixed size without maintaining the aspect ratio.
                    // Normally an image will be resized to fit inside the specified size.
                    //size.IgnoreAspectRatio = true;

                    image.Resize(size);
                    image.Write($"{imagesLocation}/Thumbnails/{imageName}");
                }
            }
            catch (AccountException e)
            {
                //Key Expired for the rest of this month
                var currentKey = keys.FirstOrDefault(k => k.Value);
                var newKey = keys.FirstOrDefault(k => !k.Value);

                keys[currentKey.Key] = false;
                keys[newKey.Key] = true;

                _appSettings.TinyPngKeys = keys;

                await ProcessImage(imageBuffer, imagesLocation, imageName, watermarkLocation);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        private string GetImageFormat(byte[] fileContent)
        {
            return "jpg";
        }

        public List<string> GetUserImageNames(long userId, bool? safeOnly = null)
        {
            User user = ValidatedSearch(u => u.UserId == userId);

            return GetUserImageNames(user, safeOnly);
        }

        public List<string> GetUserImageNames(User user, bool? safeOnly = null)
        {
            if (!Directory.Exists(user.AlbumUrl)) return new List<string>();

            var files = Directory.EnumerateFiles(user.AlbumUrl, "*.png", SearchOption.TopDirectoryOnly).ToList();
            files.AddRange(Directory.EnumerateFiles(user.AlbumUrl, "*.jpg", SearchOption.TopDirectoryOnly));

            List<string> filenames = new List<string>();

            foreach (var f in files)
            {
                string fileName = Path.GetFileName(f);

                if (safeOnly == true) if (fileName.Contains("-xxx.")) continue;
                if (safeOnly == false) if (!fileName.Contains("-xxx.")) continue;

                filenames.Add(fileName);
            };

            return filenames;
        }

        public bool DeleteUserImage(long userId, string imageName)
        {
            User user = ValidatedSearch(u => u.UserId == userId);

            string file = $"{user.AlbumUrl}/{imageName}";

            if (!File.Exists(file)) return false;
            File.Delete(file);

            file = $"{user.AlbumUrl}/Original/{imageName}";
            if (!File.Exists(file)) return false;
            File.Delete(file);

            file = $"{user.AlbumUrl}/Thumbnails/{imageName}";
            if (!File.Exists(file)) return false;
            File.Delete(file);

            return true;
        }

        public void MarkImageExplicit(long userId, string imageName)
        {
            User user = ValidatedSearch(u => u.UserId == userId);

            if (imageName.Contains("-xxx.")) return;

            var imageNameParts = imageName.Split(new char[] { '-', '_', '.' }).ToList();

            // Reset old naming convention ({userId}-profile.{extension}) to current convention
            if (imageNameParts.Count < 5)
            {
                string imageExtension = imageNameParts.LastOrDefault();

                EditImageName(userId, imageName, $"{user.FirstName}-{_appSettings.ApplicationName}-{DateTime.Now.ToString("ddMMyyHHmmss")}-{user.UserId}-xxx.{imageExtension}");

                return;
            }

            imageNameParts.Insert(imageNameParts.Count - 1, "xxx");

            string newImageName = "";

            for (int i = 0; i < imageNameParts.Count; i++)
            {
                string seperator = "-";

                if (i + 2 == imageNameParts.Count) seperator = ".";
                if (i + 1 == imageNameParts.Count) seperator = "";
                newImageName += $"{imageNameParts[i]}{seperator}";
            }

            EditImageName(userId, imageName, newImageName);
        }

        public void MarkImageSafe(long userId, string imageName)
        {
            User user = ValidatedSearch(u => u.UserId == userId);

            if (!imageName.Contains("-xxx.")) return;

            var imageNameParts = imageName.Split(new char[] { '-', '_', '.' }).ToList();

            imageNameParts.RemoveAt(imageNameParts.Count - 2);

            string newImageName = "";

            for (int i = 0; i < imageNameParts.Count; i++)
            {
                string seperator = "-";

                if (i + 2 == imageNameParts.Count) seperator = ".";
                if (i + 1 == imageNameParts.Count) seperator = "";
                newImageName += $"{imageNameParts[i]}{seperator}";
            }

            EditImageName(userId, imageName, newImageName);
        }

        public bool EditImageName(long userId, string oldImageName, string newImageName)
        {
            User user = ValidatedSearch(u => u.UserId == userId);

            string file = $"{user.AlbumUrl}/{oldImageName}";
            string newFile = $"{user.AlbumUrl}/{newImageName}";
            if (!File.Exists(file)) return false;
            //File.Copy(file, newFile, true);
            File.Move(file, newFile);

            file = $"{user.AlbumUrl}/Original/{oldImageName}";
            newFile = $"{user.AlbumUrl}/Original/{newImageName}";
            if (!File.Exists(file)) return false;
            File.Copy(file, newFile, true);

            file = $"{user.AlbumUrl}/Thumbnails/{oldImageName}";
            newFile = $"{user.AlbumUrl}/Thumbnails/{newImageName}";
            if (!File.Exists(file)) return false;
            File.Copy(file, newFile, true);

            return true;
        }

        public async Task SendFeedback(string email, string fullName, string msisdn, string message)
        {
            string body = await _email.GetEmailBody("SP.Services.Core.Management.feedback_email_template.html",
                new kvp("{Msisdn}", msisdn),
                new kvp("{UserEmail}", email),
                new kvp("{FullName}", fullName),
                new kvp("{Message}", message),
                new kvp("{CarePhone}", _appSettings.SupportMsisdn),
                new kvp("{CareEmail}", _appSettings.SupportEmail));

            await _email.Send(new List<string> { _appSettings.SupportEmail }, $"Feedback via {_appSettings.ApplicationName}", body);
        }

        public async Task UpdateDateCreated()
        {
            var users = GetAll().ToList();

            users.ForEach(u =>
            {
                if (Directory.Exists(u.AlbumUrl))
                {
                    u.DateCreated = u.DateModified = Directory.GetCreationTime(u.AlbumUrl);
                }
            });

            await SaveAsync();
        }
    }
}
