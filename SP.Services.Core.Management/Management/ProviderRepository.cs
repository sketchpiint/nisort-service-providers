﻿using System;
using Npgsql;
//using LinqKit;
using MoreLinq;
using System.Net;
using System.Linq;
using System.Text;
//using System.Linq.Dynamic;
using PostgreSQLCopyHelper;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.Helper;
using SP.Services.Core.Management.Logging;
using SP.Services.Core.Management.Extensions;
using SP.Services.Core.Management.Communication;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.RepositoryBases;

using dynamicExpression = System.Linq.Dynamic.DynamicExpression;
using kvp = System.Collections.Generic.KeyValuePair<string, string>;
using SP.Services.Core.Management.CRM;

namespace SP.Services.Core.Management
{

    public class ProviderRepository : EFCoreObjectRepository<Provider>, IProviderRepository
    {
        IServiceRepository _serviceRepository;
        IEmail _email;
        IZoho _zoho;
        IApplicationSettings _appSettings;
        IUserRepository _userRepository;

        //List<Provider> filteredProviders = new List<Provider>();

        public ProviderRepository(ServiceContext context, IServiceRepository serviceRepository, IEmail email, IApplicationSettings appSettings, IUserRepository userRepository, IZoho zoho) : base(context)
        {
            _serviceRepository = serviceRepository;
            _email = email;
            _zoho = zoho;
            _appSettings = appSettings;
            _userRepository = userRepository;
        }

        public ServiceContext Context { get { return (ServiceContext)_context; } }

        public async Task<Provider> CreateProvider(long userId, string boundingBoxJson, string generalLocation, MobilityTypes mobilityType, ProviderTypes providerType, string chestCentimeters, string heightCentimeters, DateTime dateOfBirth, string eyeColor, string hairColor, string hairType, string nationality, string physique, string waistCentimeters, string languages, string area, string town, bool suspended, string bio, string availability, string metaDataJson, int weight)
        {
            User user = _userRepository.ValidatedSearch(u => u.UserId == userId, checkActive: true);
            ValidatedSearch(p => p.UserId == userId, checkActive: true, inverseCheck: true);

            //string[] locationInfo = generalLocation.Split(',');
            //string town = "", area = "";
            //if (locationInfo.Any())
            //{
            //    area = locationInfo[0];
            //    town = locationInfo[1];
            //}

            Provider provider = new Provider
            {
                Active = true,
                UserId = user.UserId,
                MobilityType = MobilityTypes.Mobile,
                ProviderType = ProviderTypes.Individual,
                BoundingBoxJson = boundingBoxJson,
                GeneralLocation = generalLocation,
                Town = town,
                Area = area,
                ChestCentimeters = chestCentimeters,
                DateOfBirth = dateOfBirth,
                EyeColor = eyeColor,
                HairColor = hairColor,
                HairType = hairType,
                HeightCentimeters = heightCentimeters,
                Nationality = nationality,
                Physique = physique,
                WaistCentimeters = waistCentimeters,
                Languages = languages,
                Suspended = suspended,
                Bio = bio,
                Availability = availability,
                MetaDataJson = metaDataJson,
                Weight = weight
            };

            await Context.Providers.AddAsync(provider);
            user.IsProvider = true;
            //await SaveAsync();

            BillingAccount account = new BillingAccount
            {
                BillingTimerPaused = _appSettings.GracePeriodDays > 0,
                DatePaused = _appSettings.GracePeriodDays > 0 ? DateTime.Now : (DateTime?)null,
                DaysToCountDown = _appSettings.GracePeriodDays,
                NextPaymentDate = _appSettings.GracePeriodDays > 0 ? DateTime.Now.AddDays(_appSettings.GracePeriodDays) : DateTime.Now,
                PaymentDate = null,
                PausedBy = _appSettings.GracePeriodDays > 0 ? "System - Grace Period" : "",
                Provider = provider,
                ResetOnResume = false,
                AccountBalance = 0
            };

            await Context.BillingAccounts.AddAsync(account);

            await SaveAsync();

            //await _zoho.AddAndConvertLead(user.UserId, provider.ProviderId, user.Username, user.Msisdn, user.FirstName, generalLocation);

            return provider;
        }

        public async Task<Provider> EditProvider(Provider newDetails)
        {
            long providerId = newDetails.ProviderId;
            Provider provider = ValidatedSearch(p => p.ProviderId == providerId);

            string[] locationInfo = newDetails.GeneralLocation.Split(',');
            string town = "", area = "";



            if (locationInfo.Any())
            {
                area = locationInfo[0];
                town = locationInfo.Count() > 1 ? locationInfo[1] : "";
            }

            provider.Active = newDetails.Active;
            provider.Weight = newDetails.Weight;
            provider.Suspended = newDetails.Suspended;
            provider.Town = provider.Town.Coalesce(town);
            provider.Area = provider.Area.Coalesce(area);
            provider.DateOfBirth = newDetails.DateOfBirth;
            provider.Bio = provider.Bio.Coalesce(newDetails.Bio);
            provider.Physique = provider.Physique.Coalesce(newDetails.Physique);
            provider.EyeColor = provider.EyeColor.Coalesce(newDetails.EyeColor);
            provider.HairType = provider.HairType.Coalesce(newDetails.HairType);
            provider.Languages = provider.Languages.Coalesce(newDetails.Languages);
            provider.HairColor = provider.HairColor.Coalesce(newDetails.HairColor);
            provider.Nationality = provider.Nationality.Coalesce(newDetails.Nationality);
            provider.Availability = provider.Availability.Coalesce(newDetails.Availability);
            provider.MetaDataJson = provider.MetaDataJson.Coalesce(newDetails.MetaDataJson);
            provider.BoundingBoxJson = provider.BoundingBoxJson.Coalesce(newDetails.BoundingBoxJson);
            provider.GeneralLocation = provider.GeneralLocation.Coalesce(newDetails.GeneralLocation);
            provider.WaistCentimeters = provider.WaistCentimeters.Coalesce(newDetails.WaistCentimeters);
            provider.ChestCentimeters = provider.ChestCentimeters.Coalesce(newDetails.ChestCentimeters);
            provider.HeightCentimeters = provider.HeightCentimeters.Coalesce(newDetails.HeightCentimeters);

            provider.MobilityType = newDetails.MobilityType <= 0 ? provider.MobilityType : newDetails.MobilityType;
            provider.ProviderType = newDetails.ProviderType <= 0 ? provider.ProviderType : newDetails.ProviderType;

            await SaveAsync();

            return provider;
        }

        public void BulkInsertProviders(List<Provider> providers)
        {
            try
            {
                var copyHelper = new PostgreSQLCopyHelper<Provider>(nameof(Context.Providers))
                    .MapBoolean(nameof(Provider.Active), p => p.Active)
                    .MapVarchar(nameof(Provider.BoundingBoxJson), p => p.BoundingBoxJson)
                    .MapInteger(nameof(Provider.MobilityType), p => (int)p.MobilityType)
                    .MapInteger(nameof(Provider.ProviderType), p => (int)p.ProviderType);

                using (var connection = new NpgsqlConnection(Context.ConnectionString))
                {
                    connection.Open();

                    copyHelper.SaveAll(connection, providers);
                }
            }
            catch (Exception ex)
            {
                throw new CustomException($"{ex}", "An error occurred inserting provider list. Please notify system administrator", HttpStatusCode.BadRequest);
            }
        }

        public async Task AddProviderService(long providerId, int serviceId)
        {
            Provider provider = ValidatedSearch(p => p.ProviderId == providerId);
            _serviceRepository.ValidatedSearch(s => s.ServiceId == serviceId);

            ProviderService providerService = new ProviderService { ProviderId = providerId, ServiceId = serviceId };
            await Context.ProviderServices.AddAsync(providerService);
            await SaveAsync();
        }

        public async Task AddProviderService(List<ProviderService> providerServices)
        {
            providerServices.ForEach(async ps =>
            {
                var providerId = ps.ProviderId;
                var serviceId = ps.ServiceId;

                ValidatedSearch(p => p.ProviderId == providerId);
                _serviceRepository.ValidatedSearch(s => s.ServiceId == serviceId);

                ProviderService providerService = Context.ProviderServices.FirstOrDefault(eps => eps.ProviderId == providerId && eps.ServiceId == serviceId);

                if (providerService == null)
                {
                    providerService = new ProviderService { ProviderId = ps.ProviderId, ServiceId = ps.ServiceId };
                    await Context.ProviderServices.AddAsync(providerService);
                }
                else
                {
                    providerService.Active = true;
                    await SaveAsync();
                }
            });

            await SaveAsync();
        }
        public async Task RemoveProviderService(long providerId, int serviceId)
        {
            Provider provider = ValidatedSearch(p => p.ProviderId == providerId);
            _serviceRepository.ValidatedSearch(s => s.ServiceId == serviceId);

            ProviderService providerService = Context.ProviderServices.FirstOrDefault(ps => ps.ProviderId == providerId && ps.ServiceId == serviceId);
            if (providerService == null) return;

            providerService.Active = false;
            await SaveAsync();
        }

        public async Task<List<ProviderService>> GetProviderServices(long providerId)
        {
            ValidatedSearch(p => p.ProviderId == providerId);
            return await Task.Run(() => Context.ProviderServices.Include(p => p.Service).Where(pd => pd.ProviderId == providerId && pd.Active).ToList());
        }

        public async Task<List<Service>> GetServices(long providerId)
        {
            List<ProviderService> providerServices = await GetProviderServices(providerId);

            List<Service> services = new List<Service>();

            services = providerServices.Select(ps => ps.Service).ToList();

            return services;
        }

        public async Task<(List<ProviderService> services, int count)> GetProviderServices(int pageSize, int startIndex = 0, string orderByClause = "ProviderServiceId DESC")
        {
            int count = Context.ProviderServices.Count();
            List<ProviderService> providerServices =
                await Context.ProviderServices.Where(ps => ps.Active)
                .OrderBy(orderByClause).Skip(startIndex).Take(pageSize)
                .Include(ps => ps.Provider)
                    .ThenInclude(p => p.User)
                .Include(ps => ps.Service).ToListAsync();

            return (providerServices, count);
        }

        public async Task<(List<(Provider provider, List<Service> services)> providerServices, int count)> GetProvidersWithServices1(int pageSize, int startIndex = 0, string orderByClause = "ProviderServiceId DESC")
        {

            int count = Context.ProviderServices.Where(ps => ps.Active).GroupBy(ps => ps.ProviderId).Count();

            var providerServices = await GetProviderServices(pageSize, startIndex, orderByClause);

            List<Provider> providers = providerServices.services.Select(d => d.Provider).DistinctBy(p => p.ProviderId).ToList();

            List<(Provider, List<Service>)> results = new List<(Provider, List<Service>)>();

            providers.ForEach(p => results.Add((p, providerServices.services.Where(ps => ps.Provider.ProviderId == p.ProviderId).Select(d => d.Service).ToList())));

            return (results, count);
        }

        public async Task<(List<(Provider provider, List<Service> services)> providerServices, int count)> GetProvidersWithServices(int pageSize, int startIndex = 0, string orderByClause = "ProviderServiceId DESC")
        {
            int count = Context.Providers.Include(p => p.User).Where(p => p.Active && !p.Suspended && p.User.Active).Count();

            List<Provider> providers = await Context.Providers.Include(p => p.User).Where(p => p.Active && !p.Suspended && p.User.Active).OrderBy(orderByClause).Skip(startIndex).Take(pageSize).ToListAsync();

            List<(Provider, List<Service>)> results = new List<(Provider, List<Service>)>();

            providers.ForEach(p => results.Add((p, Context.ProviderServices.Where(ps => ps.Active && ps.ProviderId == p.ProviderId).Select(ps => ps.Service).ToList())));

            return (results, count);
        }

        public async Task<(int TotalCount, List<Provider> Providers)> GetProviders(int serviceId, int startIndex, int pageSize, string orderBy = "ProviderId DESC")
        {
            int totalCount = await Context.ProviderServices.Include(ps => ps.Provider).ThenInclude(p => p.User).Where(ps => ps.ServiceId == serviceId && ps.Active && !ps.Provider.Suspended && ps.Provider.Active && ps.Provider.User.Active).CountAsync();

            List<ProviderService> providerServices = await Context.ProviderServices.Include(ps => ps.Provider).ThenInclude(p => p.User).Where(ps => ps.ServiceId == serviceId && ps.Active && !ps.Provider.Suspended && ps.Provider.Active && ps.Provider.User.Active).OrderBy(orderBy).Skip(startIndex).Take(pageSize).ToListAsync();
            //Task.Run(() => );

            //var providers = await Context.ProviderServices.Include(ps => ps.Provider).Where(ps => ps.ServiceId == serviceId && ps.Active).OrderBy(orderBy).Skip(startIndex).Take(pageSize).Select(r => r.Provider).Include(p=>p.User).Where(p => !p.Suspended && p.Active).ToListAsync();


            var providers = providerServices.Select(r => r.Provider).ToList();

            return (totalCount, providers);
        }

        public async Task<(List<Provider> data, int totalCount)> GetProviders(string area, int pageSize, int startIndex = 0, string orderByClause = "ProviderId DESC")
        {
            int totalCount = await Context.Providers.Where(p => p.Area.ToLower().Contains(area.ToLower())).CountAsync();
            var providers = await Context.Providers.Include(p => p.User).Where(p => !p.Suspended && p.Active && p.Area.ToLower().Contains(area.ToLower())).OrderBy(orderByClause).Skip(startIndex).Take(pageSize).ToListAsync();

            return (providers, totalCount);
        }

        public async Task<(List<Provider> data, int totalCount)> GetProvidersByTown(string town, int pageSize, int startIndex = 0, string orderByClause = "ProviderId DESC")
        {
            int totalCount = await Context.Providers.Where(p => p.Area.ToLower().Contains(town.ToLower())).CountAsync();
            var providers = await Context.Providers.Include(p => p.User).Where(p => !p.Suspended && p.Active && p.Area.ToLower().Contains(town.ToLower())).OrderBy(orderByClause).Skip(startIndex).Take(pageSize).ToListAsync();

            return (providers, totalCount);
        }

        public List<string> GetRegisteredAreas() => Context.Providers.Select(p => p.Area).DistinctBy(p => p).ToList();

        public List<string> GetRegisteredTowns() => Context.Providers.Select(p => p.Town).DistinctBy(p => p).ToList();

        public (int TotalCount, List<Provider> Providers) Filter(List<string> locations, int minimumAge, int maximumAge, string name, List<int> serviceIds, int minimumHeightCm, int maximumHeightCm, int pageSize, int startIndex, string orderByClause, DateTime? addedAfter = null, DateTime? addedBefore = null, string username = "", bool? suspended = false, bool admin = false)
        {
            IQueryable<ProviderService> query; // = Context.Set<ProviderService>().Include(ps => ps.Provider).ThenInclude(p => p.User).Include(ps => ps.Service).Where(ps => ps.Active && !ps.Provider.Suspended == suspended && ps.Provider.Active && ps.Provider.User.Active);

            query = suspended == null ?
                Context.Set<ProviderService>().Include(ps => ps.Provider).ThenInclude(p => p.User).Include(ps => ps.Service).Where(ps => ps.Active && ps.Provider.Active && ps.Provider.User.Active)
                :
                Context.Set<ProviderService>().Include(ps => ps.Provider).ThenInclude(p => p.User).Include(ps => ps.Service).Where(ps => ps.Active && ps.Provider.Suspended == suspended && ps.Provider.Active && ps.Provider.User.Active);



            if (locations != null && locations.Any())
            {
                string locationPredicateString = "";
                int index = 0;
                locations.ForEach(l =>
                {
                    string suffix = "";
                    if (++index != locations.Count()) suffix = " OR ";
                    locationPredicateString += $@"Provider.Town == ""{l}"" OR Provider.Area == ""{l}""{suffix}";
                });

                query = query.Where(locationPredicateString);
            }

            if (!name.IsNull()) query = query.Where(ps => ps.Provider.User.FirstName.ToLower().Contains(name.ToLower()));
            if (!username.IsNull()) query = query.Where(ps => ps.Provider.User.Username.ToLower().Contains(username.ToLower()));

            if (addedBefore != null)
            {
                query = query.Where(ps => ps.Provider.User.DateCreated <= addedBefore);
            }

            if (addedAfter != null)
            {
                query = query.Where(ps => ps.Provider.User.DateCreated >= addedAfter);
            }

            DateTime minDateOfBirth = DateTime.Now.AddYears(-maximumAge);
            DateTime maxDateOfBirth = DateTime.Now.AddYears(-minimumAge);

            query = query.Where(ps => ps.Provider.DateOfBirth > minDateOfBirth && ps.Provider.DateOfBirth < maxDateOfBirth);

            if (serviceIds != null && serviceIds.Any())
            {
                var servicePredicate = PredicateBuilder.False<ProviderService>();
                serviceIds.ForEach(s => servicePredicate = servicePredicate.Or(ps => ps.ServiceId == s));
                query = query.Where(servicePredicate);
            }

            int totalCount = query.GroupBy(ps => ps.ProviderId).Count();

            List<ProviderService> result = new List<ProviderService>();

            if (admin)
            {
                //totalCount =  query.GroupBy(ps => ps.ProviderId).Count();
                result = query.DistinctBy(p => p.ProviderId).AsQueryable().OrderBy(orderByClause).Skip(startIndex).Take(pageSize).ToList();
            }
            else
                result = query.OrderBy(r => Guid.NewGuid()).DistinctBy(p => p.ProviderId).AsQueryable().Skip(startIndex).Take(pageSize).ToList();

            //var providerQuery = query.OrderBy(orderByClause).GroupBy(ps => ps.Provider).Select(ig => ig.Key);

            //var result = providerQuery.Skip(startIndex).Take(pageSize).ToList();            

            //List<Provider> providers = result.Select(ps => ps.Provider).ToList();

            //return (totalCount, providers);

            return (totalCount, result.Select(ps => ps.Provider).ToList());
        }

        public async Task Suspend(long providerId, SuspensionReasons reason, string detail, string suspendedBy)
        {
            Provider provider = ValidatedSearch(p => p.ProviderId == providerId);

            provider.Suspended = true;

            SuspensionLog log = new SuspensionLog
            {
                ProviderId = providerId,
                SuspensionReasonId = (int)reason,
                SuspensionDetail = detail,
                SuspendedBy = suspendedBy,
                SuspensionDate = DateTime.Now
            };

            await Context.SuspensionLogs.AddAsync(log);

            await SaveAsync();
        }

        public async Task Reinstate(long providerId, string reinstatedBy, string reinstateDetail)
        {
            Provider provider = ValidatedSearch(p => p.ProviderId == providerId);

            provider.Suspended = false;

            SuspensionLog log = await Context.SuspensionLogs.Where(s => s.ProviderId == providerId).OrderByDescending(s => s.SuspensionLogId).FirstOrDefaultAsync();

            log.ReinstatedBy = reinstatedBy;
            log.ReinstatedDate = DateTime.Now;
            log.ReinstateDetail = reinstateDetail;

            await SaveAsync();
        }

        public async Task<List<Provider>> GetWithNonExplicitImages(int count)
        {
            List<Provider> filteredProviders = new List<Provider>();

            return await filterForNonExplicitImages(filteredProviders, count, 0);
        }

        public async Task<List<Provider>> filterForNonExplicitImages(List<Provider> filteredProviders, int count, int startIndex = 0)
        {
            var providers = Context.Providers.Include(p => p.User).Where(p=>p.Active && !p.Suspended && p.User.Active).OrderBy("ProviderId DESC").Skip(startIndex).Take(count);

            //var providers = GetAll("ProviderId DESC", out int totalCount, count, startIndex);
            providers.ForEach(p =>
            {
                var imageNames = _userRepository.GetUserImageNames(p.User);

                if (imageNames.Any(i => !i.Contains("-xxx."))) { filteredProviders.Add(p); }
            });

            if (filteredProviders.Count < count)
                await filterForNonExplicitImages(filteredProviders, Math.Abs(count - filteredProviders.Count), count);

            return filteredProviders;
        }
    }
}