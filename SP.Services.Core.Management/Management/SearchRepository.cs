﻿using SP.Services.Core.Management.RepositoryBases;
using System;
using System.Collections.Generic;
using System.Text;

using SP.Services.Core.Engine.Database;
using SP.Services.Core.Management.Helper;
using SP.Services.Core.Management.Configuration;
using System.Threading.Tasks;

namespace SP.Services.Core.Management
{
    public class SearchRepository : FunctionalRepositoryBase, ISearchRepository
    {
        IApplicationSettings _settings;
        public SearchRepository(ServiceContext _context, IApplicationSettings settings) : base(_context, settings)
        {
            _settings = settings;
        }

        public async Task<string> Search(string query) => await HelperRepository.GetWebData($"{_settings.FusionApiUrl}?{query}", _settings.FusionUserName, _settings.FusionPassword);
    }
}
