﻿using System.Threading.Tasks;

namespace SP.Services.Core.Management
{
    public interface ISearchRepository
    {
        Task<string> Search(string query);
    }
}