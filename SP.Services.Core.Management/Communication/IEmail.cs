﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SP.Services.Core.Management.Communication
{
    public interface IEmail
    {
        Task Send(List<string> to, string subject, string body, string from = "", string password = "");
        Task<string> GetEmailBody(string template, params KeyValuePair<string, string>[] placeholderValues);
        Task<string> GetEmailBody(string template, Dictionary<string, string> placeholderValues);
    }
}