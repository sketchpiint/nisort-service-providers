﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SP.Services.Core.Management.DTOs;

namespace SP.Services.Core.Management.Communication
{
    public interface ISms
    {
        Task Send(List<SmsPayload> payload);
        Task Send(SmsPayload payload);
    }
}