﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SP.Services.Core.Management.RepositoryBases;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.DTOs;
using SP.Services.Core.Management.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SP.Services.Core.Management.Communication
{
    public class Sms : FunctionalRepositoryBase, ISms
    {
        IHelperRepository _helperRepository;

        public Sms(ServiceContext context, IApplicationSettings settings, IHelperRepository helperRepository) : base(context, settings)
        {
            _helperRepository = helperRepository;
        }

        public async Task Send(SmsPayload payload)
        {
            string resultJson = await HelperRepository.PostWebData(_applicationSettings.SmsApiUrl + "SendSingleSms", JsonConvert.SerializeObject(payload), _applicationSettings.SmsApiUsername, _applicationSettings.SmsApiPassword, true);

            dynamic result = JArray.Parse(resultJson); //JsonConvert.DeserializeObject(resultJson);

        }

        public async Task Send(List<SmsPayload> payload)
        {
            string result = await HelperRepository.PostWebData(_applicationSettings.SmsApiUrl + "SendMultipleSms", JsonConvert.SerializeObject(new { payload = payload }), _applicationSettings.SmsApiUsername, _applicationSettings.SmsApiPassword, true);
        }
    }
}
