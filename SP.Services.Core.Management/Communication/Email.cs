﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SP.Services.Core.Management.RepositoryBases;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Management.Configuration;

namespace SP.Services.Core.Management.Communication
{
    public class Email : FunctionalRepositoryBase, IEmail
    {
        public Email(ServiceContext _context, IApplicationSettings settings) : base(_context, settings)
        {
        }

        public async Task Send(List<string> to, string subject, string body, string from = "", string password = "")
        {
            try
            {
                string fromUsername = _applicationSettings.SystemEmailSender;// "sketchpiint@gmail.com";

                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(fromUsername));
                to.ForEach(t => message.To.Add(new MailboxAddress(t)));
                message.Subject = subject;

                var bodyBuilder = new BodyBuilder();
                bodyBuilder.HtmlBody = body;

                message.Body = bodyBuilder.ToMessageBody(); //new TextPart("plain") { Text = body };

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(_applicationSettings.SystemEmailServer, _applicationSettings.SystemEmailPort, _applicationSettings.SystemEmailSsl ?? false);

                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(fromUsername, _applicationSettings.SystemEmailPassword);

                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception ex)
            {
                throw;
                //_logger.LogError(ex);
            }
        }

        public async Task<string> GetEmailBody(string template, Dictionary<string, string> placeholderValues)
        {
            string templateContent = await GetEmbeddedResourceAsync(template);

            foreach (var ph in placeholderValues)
            {
                templateContent = templateContent.Replace(ph.Key, ph.Value);
            }

            return templateContent;
        }

        public async Task<string> GetEmailBody(string template, params KeyValuePair<string, string>[] placeholderValues)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            placeholderValues.ToList().ForEach(ph => dictionary.Add(ph.Key, ph.Value));
            return await GetEmailBody(template, dictionary);
        }

        public async Task<string> GetEmbeddedResourceAsync(string resourceName)
        {
            Assembly assembly = typeof(Email).GetTypeInfo().Assembly;

            Stream resStream = assembly.GetManifestResourceStream(resourceName);

            using (var reader = new StreamReader(resStream))
            {
                return await reader.ReadToEndAsync();
            }
        }
    }
}
