﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SP.Services.Core.Management.DTOs
{
    public class SmsRecipient
    {
        public string Msisdn { get; set; }
        public string Third_party_message_id { get; set; }
    }
}
