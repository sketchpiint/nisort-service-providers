﻿using System.Collections.Generic;

namespace SP.Services.Core.Management.DTOs
{
    public class SmsPayload
    {
        public string phoneNumber { get; set; }
        public string Sender { get; set; }
        public List<SmsRecipient> Recipients { get; set; }
        public string Message { get; set; }
    }
}
