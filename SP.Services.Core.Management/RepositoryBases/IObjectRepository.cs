using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SP.Services.Core.Management.RepositoryBases
{
    public interface IObjectRepository<TEntity> where TEntity : class
    {
        TEntity Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        TEntity ValidatedFind(Expression<Func<TEntity, bool>> predicate, bool throwException = true, bool inverseCheck = false);
        TEntity ValidatedSearch(Expression<Func<TEntity, bool>> predicate, bool checkActive = false, string activePropertyName = "Active", bool throwException = true, bool inverseCheck = false);
        TEntity Find(IEnumerable<object> ids);
        TEntity Find<T>(T id) where T : struct;
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll(string orderByExpression, out int totalCount, int pageSize = 1, int startIndex = 0);
        TEntity Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> Search(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Search(Expression<Func<TEntity, bool>> predicate, string orderByExpression, int pageSize = 1, int startIndex = 0);
        IEnumerable<TEntity> Search(Expression<Func<TEntity, bool>> predicate, string orderByExpression, out int totalCount, int pageSize = 1, int startIndex = 0);
        TEntity ChangeActiveStatus(Expression<Func<TEntity, bool>> predicate, bool active, string propertyName = "Active");
        void Save();
    }
}
