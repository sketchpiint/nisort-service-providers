﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Database;
using SP.Services.Core.Management.Configuration;

namespace SP.Services.Core.Management.RepositoryBases
{
    public abstract class FunctionalRepositoryBase
    {
        protected static IApplicationSettings _applicationSettings = null;
        protected ServiceContext context = null;

        public FunctionalRepositoryBase(ServiceContext _context, IApplicationSettings settings)
        {
            context = _context;
            _applicationSettings = settings;
        }
    }
}
