﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using SP.Services.Core.Management.Logging;
using Microsoft.EntityFrameworkCore;

using kvp = System.Collections.Generic.KeyValuePair<string, string>;
using System.Threading.Tasks;

namespace SP.Services.Core.Management.RepositoryBases
{

    public abstract class EFCoreObjectRepository<TEntity> : IObjectRepository<TEntity> where TEntity : class
    {
        protected DbContext _context = null;

        private LinkedList<kvp> propertyNamesList = new LinkedList<kvp>();

        string entityName = typeof(TEntity).Name;

        public EFCoreObjectRepository(DbContext context)
        {
            _context = context;
        }

        public TEntity Add(TEntity entity) => _context.Set<TEntity>().Add(entity).Entity;

        public void AddRange(IEnumerable<TEntity> entities) => _context.Set<TEntity>().AddRange(entities);

        public TEntity Find(IEnumerable<object> ids) => _context.Set<TEntity>().Find(ids);

        public TEntity Find<T>(T id) where T : struct => _context.Set<TEntity>().Find(id);

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate) => _context.Set<TEntity>().FirstOrDefault(predicate);

        public IEnumerable<TEntity> GetAll() => _context.Set<TEntity>().ToList();

        public IEnumerable<TEntity> GetAll(string orderByExpression, out int totalCount, int pageSize = 1, int startIndex = 0)
        {
            totalCount = _context.Set<TEntity>().Count();
            return _context
                .Set<TEntity>()
                .OrderBy(orderByExpression)                
                .Skip(startIndex)
                .Take(pageSize);
        }

        public TEntity Remove(TEntity entity) => _context.Set<TEntity>().Remove(entity).Entity;

        public void RemoveRange(IEnumerable<TEntity> entities) => _context.Set<TEntity>().RemoveRange(entities);

        public void Save() => _context.SaveChanges();

        public async Task SaveAsync() => await _context.SaveChangesAsync();

        public IEnumerable<TEntity> Search(Expression<Func<TEntity, bool>> predicate) => _context.Set<TEntity>().Where(predicate);

        public IEnumerable<TEntity> Search(Expression<Func<TEntity, bool>> predicate, string orderByExpression, int pageSize = 1, int startIndex = 0)
        {
            return _context
                .Set<TEntity>()
                .Where(predicate)
                .OrderBy(orderByExpression)
                .Skip(startIndex)
                .Take(pageSize);
        }

        public IEnumerable<TEntity> Search(Expression<Func<TEntity, bool>> predicate, string orderByExpression, out int totalCount, int pageSize = 1, int startIndex = 0)
        {
            totalCount = _context.Set<TEntity>()
                .Where(predicate)
                .Count();

            return _context
                .Set<TEntity>()
                .Where(predicate)
                .OrderBy(orderByExpression)
                .Skip(startIndex)
                .Take(pageSize);
        }

        /// <summary>
        /// Gets the TEntity by the predicate from the database
        /// </summary>
        /// <param name="predicate">Binary expression defining the criteria by which to get the TEntity</param>
        /// <param name="checkActive">Checks if TEntity is active. Returns null or throws exception depending on <paramref name="throwException"/> </param>
        /// <param name="activePropertyName">The name of the property to check. Provides for different names, e.g Active, Enabled, e.t.c</param>
        /// <param name="throwException">True: Handle invalid status. False: Ignore invalid status and assume caller will handle it</param>
        /// <param name="inverseCheck">True: If TEntity exists throws exception or return TEntity <paramref name="throwException"/>. False: Ignore that TEntity exists</param>
        /// <returns>Returns user</returns>
        public TEntity ValidatedSearch(Expression<Func<TEntity, bool>> predicate, bool checkActive = false, string activePropertyName = "Active", bool throwException = true, bool inverseCheck = false)
        {
            propertyNamesList = new LinkedList<kvp>();

            GetPropertyName(predicate.Body);


            TEntity entity = FirstOrDefault(predicate);

            if (entity != null && inverseCheck)
                throw new CustomException($"{GetPropertiesString()} is already registered", HttpStatusCode.Conflict);

            if (entity == null && !inverseCheck)
            {
                if (throwException)
                    throw new CustomException($"{entityName} with {GetPropertiesString()} could not be found", HttpStatusCode.NotFound);

                return null;
            }

            if (checkActive && entity != null)
            {
                PropertyInfo property = typeof(TEntity).GetProperty(activePropertyName);
                if (property != null)
                {
                    if (property.GetValue(entity).ToString().ToUpper() != "True".ToUpper())
                    {
                        if (throwException)
                            throw new CustomException($"{entityName} with {GetPropertiesString()} has been deactivated", HttpStatusCode.Gone);
                        //throw new IdentityException($"{nameof(TEntity)} with {propertyName}, '{propertyValue}' has been deactivated", HttpStatusCode.Gone);
                        return null;
                    }
                }
            }

            return entity;
        }

        /// <summary>
        /// Gets the user by their username from the database
        /// </summary>
        /// <param name="username">The username of the user to retrieve from the database</param>
        /// <param name="checkActive">Checks if user is active. Returns null or throws exception depending on <paramref name="throwException"/> </param>
        /// <param name="throwException">True: Handle invalid status. False: Ignore invalid status and assume caller will handle it</param>
        /// <param name="inverseCheck">True: If user exists throws exception or return user <paramref name="throwException"/>. False: Ignore that the user exists</param>
        /// <returns>Returns user</returns>
        public TEntity ValidatedFind(Expression<Func<TEntity, bool>> predicate, bool throwException = true, bool inverseCheck = false)
        {
            propertyNamesList = new LinkedList<kvp>();

            string propertyName = "";
            string propertyValue = "";

            //GetPropertyName1(predicate, out propertyName, out propertyValue);
            GetPropertyName(predicate.Body);

            TEntity entity = FirstOrDefault(predicate);

            if (entity != null && inverseCheck)
                throw new CustomException($"{GetPropertiesString()} is already registered", HttpStatusCode.Conflict);

            if (entity == null)
            {
                if (throwException)
                    throw new CustomException($"{entityName} with {GetPropertiesString()} could not be found", HttpStatusCode.NotFound);
                return null;
            }

            return entity;
        }

        private kvp GetPropertyName<TSource, TProperty>(TSource source, Expression<Func<TSource, TProperty>> propertyLambda)
        {
            string propertyName = "";
            string propertyValue = "";

            GetPropertyName1(propertyLambda, out propertyName, out propertyValue);

            return new kvp(propertyName, propertyValue);
        }

        private void GetPropertyName1<TSource, TProperty>(Expression<Func<TSource, TProperty>> propertyLambda, out string propertyName, out string propertyValue)
        {
            BinaryExpression expression = ((BinaryExpression)propertyLambda.Body);
            MemberInfo member = ((MemberExpression)expression.Left).Member;
            string name = member.Name;
            Expression value = expression.Right;


            if (member.IsDefined(typeof(DisplayNameAttribute)))
            {
                Attribute displayNameAttribute = member.GetCustomAttribute(typeof(DisplayNameAttribute));
                //Attribute.GetCustomAttribute(member, typeof(DisplayNameAttribute));
                name = ((DisplayNameAttribute)displayNameAttribute).DisplayName;
            }

            propertyName = name;
            propertyValue = value.ToString();
        }


        private void GetPropertyName(Expression expressionParameter)
        {
            string name = "", value = "";
            MemberInfo member = default(MemberInfo);

            if (expressionParameter.GetType() == typeof(UnaryExpression))
            {
                UnaryExpression expression = ((UnaryExpression)expressionParameter);
                member = ((MemberExpression)expression.Operand).Member;
                name = member.Name;
                value = expression.NodeType.ToString();
            }
            else
            {
                BinaryExpression expression = ((BinaryExpression)expressionParameter);

                if (expression.Right.GetType() != typeof(ConstantExpression)
                    && (expression.Right.GetType() != typeof(MemberExpression)
                    && !expression.Right.GetType().GetTypeInfo().IsSubclassOf(typeof(MemberExpression))))
                {
                    GetPropertyName(expression.Left);
                    GetPropertyName(expression.Right);

                    return;
                }
                member = ((MemberExpression)expression.Left).Member;
                name = member.Name;

                if (expression.Right.GetType() == typeof(ConstantExpression))
                    value = expression.Right.ToString();
                else
                {

                    var rightMemberExpression = (MemberExpression)expression.Right;
                    var constantExpression = (ConstantExpression)rightMemberExpression.Expression;
                    var fieldValue = ((FieldInfo)rightMemberExpression.Member).GetValue(constantExpression.Value);

                    value = fieldValue.ToString();
                }
            }

            if (member.IsDefined(typeof(DisplayNameAttribute)))
            {
                Attribute displayNameAttribute = member.GetCustomAttribute(typeof(DisplayNameAttribute));

                name = ((DisplayNameAttribute)displayNameAttribute).DisplayName;
            }

            propertyNamesList.AddLast(new kvp(name, value));
        }

        /// <summary>
        /// Activates or deactivates an entity.
        /// </summary>
        /// <param name="predicate">Criteria fetch the entity from the data store</param>
        /// <param name="active">Sets whether to activate or deactivate the entity</param>
        /// <param name="propertyName">The name of the property to check. Provides for different names, e.g Active, Enabled, e.t.c</param>
        /// <returns>Entity whose active status has been changed</returns>
        public TEntity ChangeActiveStatus(Expression<Func<TEntity, bool>> predicate, bool active, string propertyName = "Active")
        {
            TEntity entity = _context.Set<TEntity>().FirstOrDefault(predicate);

            PropertyInfo property = typeof(TEntity).GetProperty(propertyName);

            if (property == null)
            {
                throw new CustomException($"{entityName} does not have provision for '{propertyName}' status", HttpStatusCode.Forbidden);
            }
            if ((bool)property.GetValue(entity) == active) return entity;

            property.SetValue(entity, active);

            return entity;
        }

        private string GetPropertiesString()
        {
            string message = "";
            string seperator = propertyNamesList.Count > 1 ? "| " : "";

            foreach (var item in propertyNamesList)
            {
                message += $"{item.Key} {item.Value} ";

                if (!propertyNamesList.Last.Value.Equals(item)) message += seperator;
            }

            return message;
        }

        //ToDo: Create extension method for checking active and binary get 

        //ToDo: Evaluate need for ValidatedFind() 
    }
}