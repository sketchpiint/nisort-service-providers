﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

using SP.Services.Core.Engine.Database;
using SP.Services.Core.Management.Logging;
using SP.Services.Core.Management.Security;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.RepositoryBases;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management.Extensions;
using Exceptionless;
using Exceptionless.Json;
using Exceptionless.Logging;

namespace SP.Services.Core.Management.Helper
{
    public class HelperRepository : FunctionalRepositoryBase, IHelperRepository
    {
        public HelperRepository(ServiceContext _context, IApplicationSettings settings) : base(_context, settings)
        {
        }

        public static string UppercaseFirst(string stringToConvert)
        {
            if (string.IsNullOrEmpty(stringToConvert))
            {
                return string.Empty;
            }
            stringToConvert = stringToConvert.ToLower();

            return char.ToUpper(stringToConvert[0]) + stringToConvert.Substring(1);
        }

        public static string SentenceCase(string sentence)
        {
            if (sentence.IsNull()) return sentence; //==null || sentence.Length < 1

            string newSentence = "";

            sentence.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(w =>
            {
                string word = w.ToLower();

                if (!String.IsNullOrEmpty(newSentence)) newSentence += " ";

                newSentence += w[0].ToString().ToUpper() + w.Substring(1);
            });

            return newSentence;
        }

        public static string AddOrdinal(int number)
        {
            if (number <= 0) return number.ToString();

            switch (number % 100)
            {
                case 11:
                case 12:
                case 13:
                    return number + "th";
            }

            switch (number % 10)
            {
                case 1:
                    return number + "st";
                case 2:
                    return number + "nd";
                case 3:
                    return number + "rd";
                default:
                    return number + "th";
            }

        }

        public static bool ValidateEmailAddress(string emailAddress, bool throwException = true)
        {
            const string emailValidationRegexString = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

            if (!Regex.IsMatch(emailAddress, emailValidationRegexString, RegexOptions.IgnoreCase))
            {
                if (throwException) throw new CustomException($"The email address provided, '{emailAddress}' is invalid", HttpStatusCode.BadRequest);

                return false;
            }

            return true;
        }

        public void EvaluatePasswordSecurity(string password)
        {
            if (password.Length < _applicationSettings.PasswordLength) throw new CustomException($"Passwords must be at least {_applicationSettings.PasswordLength} characters long", HttpStatusCode.PreconditionFailed);
        }

        internal static void SecurePassword(string password, int workFactor, out string salt, out string hash)
        {
            byte[] saltBytes = Hashing.GenerateSalt(_applicationSettings.PasswordSaltLength);

            salt = Encoding.ASCII.GetString(saltBytes);

            hash = SecurePassword(password, salt, workFactor);

        }

        internal static string SecurePassword(string password, string salt, int workFactor)
        {
            byte[] saltBytes = Encoding.ASCII.GetBytes(salt);

            byte[] hashBytes = Hashing.PBKDF(Encoding.ASCII.GetBytes(password), saltBytes, _applicationSettings.PasswordSaltLength, workFactor);

            return Encoding.ASCII.GetString(hashBytes);
        }

        internal static int GetNewWorkFactor(int oldWorkFactor, bool newUser = false)
        {
            if (oldWorkFactor >= _applicationSettings.MaximumWorkFactor || newUser) return _applicationSettings.MinimumWorkFactor;

            return ++oldWorkFactor;
        }

        public static async Task<string> GetWebData(string queryUrl, string username, string password)
        {
            try
            {
                HttpClient client = new HttpClient();

                if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));

                string result = await client.GetStringAsync(queryUrl);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static async Task<string> GetWebData(string queryUrl, string username, string password)
        //{
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(queryUrl);
        //    string auth = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{username}:{password}"));
        //    request.Headers.Add("Authorization", $"Basic {auth}");
        //    HttpWebResponse webResponse = (HttpWebResponse)(await request.GetResponseAsync());

        //    string response = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
        //    return response;
        //}

        public static async Task<string> PostWebData(string url, string data, string username = "", string password = "", bool auth = false)
        {
            var client = new HttpClient();

            if (auth) client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));

            var response = await client.PostAsync(url, new StringContent(data, Encoding.UTF8, "application/json"));

            //response.EnsureSuccessStatusCode();

            ExceptionlessClient _exceptionlessClient = ExceptionlessClientBuilder.ExeptionlessClient;

            _exceptionlessClient.SubmitLog($"Posting to grabber; request: {_applicationSettings.GrabberUrl} | data: {data} ______ response: {response}", LogLevel.Debug);

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<SecurityLinkToken> RetrieveSecurityToken(SecurityLinkTokenTypes type, string token, bool validate = false)
        {
            Guid _token = new Guid();
            if (!Guid.TryParse(token, out _token))
                throw new CustomException("The page you attempted to visit cannot be found", HttpStatusCode.NotFound);

            return await RetrieveSecurityToken(type, _token, validate);

        }

        public async Task<SecurityLinkToken> RetrieveSecurityToken(SecurityLinkTokenTypes type, Guid token, bool validate = false)
        {
            SecurityLinkToken linkToken = context.SecurityLinkTokens.FirstOrDefault(sl => sl.LinkToken == token && sl.SecurityLinkTokenTypeId == (int)type);

            if (linkToken == null) throw new CustomException("The link you are looking for does not exist", HttpStatusCode.NotFound);

            if (linkToken.ExpiryDate != null && linkToken.ExpiryDate.Value.CompareTo(DateTime.Now) <= 0) throw new CustomException("This link is expired. Please request another link", HttpStatusCode.Forbidden);

            if (linkToken.Retrieved) throw new CustomException("This link is no longer valid. Please request another link", HttpStatusCode.Forbidden);

            if (!validate) linkToken.Retrieved = true;
            await context.SaveChangesAsync();

            return linkToken;
        }

        public async Task<Guid> RequestSecurityLinkTokenAsync(SecurityLinkTokenTypes type, string email)
        {
            DateTime dateCreated = DateTime.Now;
            DateTime? expiryDate = new DateTime();

            switch (type)
            {
                case SecurityLinkTokenTypes.SetPassword:
                    if (_applicationSettings.SetPasswordExpiryHours <= 0) expiryDate = null;
                    if (_applicationSettings.SetPasswordExpiryHours > 0)
                        expiryDate = dateCreated.AddHours(_applicationSettings.SetPasswordExpiryHours);
                    break;
                case SecurityLinkTokenTypes.ResetPassword:
                    if (_applicationSettings.ResetPasswordLinkExpiryHours <= 0) expiryDate = null;
                    if (_applicationSettings.ResetPasswordLinkExpiryHours > 0)
                        expiryDate = dateCreated.AddHours(_applicationSettings.ResetPasswordLinkExpiryHours);
                    break;
                case SecurityLinkTokenTypes.Invitation:
                    if (_applicationSettings.InvitationLinkExpiryHours <= 0) expiryDate = null;
                    if (_applicationSettings.InvitationLinkExpiryHours > 0)
                        expiryDate = dateCreated.AddHours(_applicationSettings.InvitationLinkExpiryHours);
                    break;
                default:
                    break;
            }

            Guid token = Guid.NewGuid();

            SecurityLinkToken linkToken = new SecurityLinkToken
            {
                DateCreated = dateCreated,
                ExpiryDate = expiryDate,
                Email = email,
                Retrieved = false,
                SecurityLinkTokenTypeId = (int)type,
                LinkToken = token
            };

            await context.AddAsync(linkToken);
            await context.SaveChangesAsync();

            return token;
        }

        public static (int feet, double inches) GetFeetInches(double centimeters, bool roundOff = true) => ((int)(centimeters / 2.54) / 12, roundOff ? Math.Round((centimeters / 2.54) % 12) : ((centimeters / 2.54) % 12));

        public static double GetCentimeters(int feet, double inches) => ((feet * 12) + inches) * 2.54;
    }
}
