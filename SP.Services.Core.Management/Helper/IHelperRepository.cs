﻿using System;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Engine.Enums;

namespace SP.Services.Core.Management.Helper
{
    public interface IHelperRepository
    {
        void EvaluatePasswordSecurity(string password);
        Task<Guid> RequestSecurityLinkTokenAsync(SecurityLinkTokenTypes type, string email);
        Task<SecurityLinkToken> RetrieveSecurityToken(SecurityLinkTokenTypes type, Guid token, bool validate = false);
        Task<SecurityLinkToken> RetrieveSecurityToken(SecurityLinkTokenTypes type, string token, bool validate = false);
    }
}