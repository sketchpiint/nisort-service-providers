﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using SP.Services.Core.Engine.Database;

namespace SP.Services.Core.Management.Configuration
{
    public class ApplicationSettings : IApplicationSettings
    {
        private ServiceContext context = null;
        public ApplicationSettings(ServiceContext _context)
        {
            context = _context;
        }

        private int? passwordLength;
        public int PasswordLength
        {
            get
            {
                passwordLength = passwordLength ?? Int32.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "PasswordLength").SettingValue);
                return (int)passwordLength;
            }
            set
            {
                passwordLength = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "PasswordLength").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private int? passwordSaltLength;
        public int PasswordSaltLength
        {
            get
            {
                passwordSaltLength = passwordSaltLength ?? Int32.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "PasswordSaltLength").SettingValue);
                return (int)passwordSaltLength;
            }
            set
            {
                passwordSaltLength = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "PasswordSaltLength").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private int? minimumWorkFactor;
        public int MinimumWorkFactor
        {
            get
            {
                minimumWorkFactor = minimumWorkFactor ?? Int32.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "MinimumWorkFactor").SettingValue);
                return (int)minimumWorkFactor;
            }
            set
            {
                minimumWorkFactor = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "MinimumWorkFactor").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private int? maximumWorkFactor;
        public int MaximumWorkFactor
        {
            get
            {
                maximumWorkFactor = maximumWorkFactor ?? Int32.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "MaximumWorkFactor").SettingValue);
                return (int)maximumWorkFactor;
            }
            set
            {
                maximumWorkFactor = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "MaximumWorkFactor").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private string fusionApiUrl;// = "http://localhost:8765/api/v1/query-pipelines/MicroServices-default/collections/MicroServices/select";
        public string FusionApiUrl
        {
            get
            {
                fusionApiUrl = fusionApiUrl ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "FusionApiUrl").SettingValue;
                return fusionApiUrl;
            }
            set
            {
                fusionApiUrl = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "FusionApiUrl").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string fusionUserName;
        public string FusionUserName
        {
            get
            {
                fusionUserName = fusionUserName ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "FusionUserName").SettingValue;
                return fusionUserName;
            }
            set
            {
                fusionUserName = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "FusionUserName").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string fusionPassword;
        public string FusionPassword
        {
            get
            {
                fusionPassword = fusionPassword ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "FusionPassword").SettingValue;
                return fusionPassword;
            }
            set
            {
                fusionPassword = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "FusionPassword").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string applicationName;
        public string ApplicationName
        {
            get
            {
                applicationName = applicationName ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ApplicationName").SettingValue;
                return applicationName;
            }
            set
            {
                applicationName = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ApplicationName").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string supportMsisdn;
        public string SupportMsisdn
        {
            get
            {
                supportMsisdn = supportMsisdn ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SupportMsisdn").SettingValue;
                return supportMsisdn;
            }
            set
            {
                supportMsisdn = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SupportMsisdn").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string supportEmail;
        public string SupportEmail
        {
            get
            {
                supportEmail = supportEmail ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SupportEmail").SettingValue;
                return supportEmail;
            }
            set
            {
                supportEmail = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SupportEmail").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string setPasswordLink;
        public string SetPasswordLink
        {
            get
            {
                setPasswordLink = setPasswordLink ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SetPasswordLink").SettingValue;
                return setPasswordLink;
            }
            set
            {
                setPasswordLink = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SetPasswordLink").SettingValue = value;
                context.SaveChanges();
            }
        }

        private short? setPasswordExpiryHours;
        public short SetPasswordExpiryHours
        {
            get
            {
                setPasswordExpiryHours = setPasswordExpiryHours ?? Int16.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SetPasswordExpiryHours").SettingValue);
                return (short)setPasswordExpiryHours;
            }
            set
            {
                setPasswordExpiryHours = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SetPasswordExpiryHours").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private string resetPasswordLink;
        public string ResetPasswordLink
        {
            get
            {
                resetPasswordLink = resetPasswordLink ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ResetPasswordLink").SettingValue;
                return resetPasswordLink;
            }
            set
            {
                resetPasswordLink = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ResetPasswordLink").SettingValue = value;
                context.SaveChanges();
            }
        }

        private short? resetPasswordLinkExpiryHours;
        public short ResetPasswordLinkExpiryHours
        {
            get
            {
                resetPasswordLinkExpiryHours = resetPasswordLinkExpiryHours ?? Int16.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ResetPasswordLinkExpiryHours").SettingValue);
                return (short)resetPasswordLinkExpiryHours;
            }
            set
            {
                resetPasswordLinkExpiryHours = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ResetPasswordLinkExpiryHours").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private string invitationLink;
        public string InvitationLink
        {
            get
            {
                invitationLink = invitationLink ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "InvitationLink").SettingValue;
                return invitationLink;
            }
            set
            {
                invitationLink = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "InvitationLink").SettingValue = value;
                context.SaveChanges();
            }
        }

        private short? invitationLinkExpiryHours;
        public short InvitationLinkExpiryHours
        {
            get
            {
                invitationLinkExpiryHours = invitationLinkExpiryHours ?? Int16.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "InvitationLinkExpiryHours").SettingValue);
                return (short)invitationLinkExpiryHours;
            }
            set
            {
                invitationLinkExpiryHours = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "InvitationLinkExpiryHours").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private string queryStringToken;
        public string QueryStringToken
        {
            get
            {
                queryStringToken = queryStringToken ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "QueryStringToken").SettingValue;
                return queryStringToken;
            }
            set
            {
                queryStringToken = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "QueryStringToken").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string systemEmailSender;
        public string SystemEmailSender
        {
            get
            {
                systemEmailSender = systemEmailSender ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailSender").SettingValue;
                return systemEmailSender;
            }
            set
            {
                systemEmailSender = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailSender").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string systemEmailPassword;
        public string SystemEmailPassword
        {
            get
            {
                systemEmailPassword = systemEmailPassword ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailPassword").SettingValue;
                return systemEmailPassword;
            }
            set
            {
                systemEmailPassword = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailPassword").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string systemEmailServer;
        public string SystemEmailServer
        {
            get
            {
                systemEmailServer = systemEmailServer ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailServer").SettingValue;
                return systemEmailServer;
            }
            set
            {
                systemEmailServer = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailServer").SettingValue = value;
                context.SaveChanges();
            }
        }

        private bool? systemEmailSsl;
        public bool? SystemEmailSsl
        {
            get
            {
                systemEmailSsl = systemEmailSsl ?? Boolean.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailSsl").SettingValue);
                return (bool)systemEmailSsl;
            }
            set
            {
                systemEmailSsl = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailSsl").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private short? systemEmailPort;
        public short SystemEmailPort
        {
            get
            {
                systemEmailPort = systemEmailPort ?? Int16.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailPort").SettingValue);
                return (short)systemEmailPort;
            }
            set
            {
                systemEmailPort = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SystemEmailPort").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private string imagesLocation;
        public string ImagesLocation
        {
            get
            {
                imagesLocation = imagesLocation ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ImagesLocation").SettingValue;
                return imagesLocation;
            }
            set
            {
                imagesLocation = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "ImagesLocation").SettingValue = value;
                context.SaveChanges();
            }
        }

        private int? maximumImageCount;
        public int MaximumImageCount
        {
            get
            {
                maximumImageCount = maximumImageCount ?? Int32.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "MaximumImageCount").SettingValue);
                return (int)maximumImageCount;
            }
            set
            {
                maximumImageCount = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "MaximumImageCount").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private int? gracePeriodDays;
        public int GracePeriodDays
        {
            get
            {
                gracePeriodDays = gracePeriodDays ?? Int32.Parse(context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "GracePeriodDays").SettingValue);
                return (int)gracePeriodDays;
            }
            set
            {
                gracePeriodDays = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "GracePeriodDays").SettingValue = value.ToString();
                context.SaveChanges();
            }
        }

        private string smsApiUrl;
        public string SmsApiUrl
        {
            get
            {
                smsApiUrl = smsApiUrl ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiUrl").SettingValue;
                return smsApiUrl;
            }
            set
            {
                smsApiUrl = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiUrl").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string smsApiUsername;
        public string SmsApiUsername
        {
            get
            {
                smsApiUsername = smsApiUsername ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiUsername").SettingValue;
                return smsApiUsername;
            }
            set
            {
                smsApiUsername = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiUsername").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string smsApiPassword;
        public string SmsApiPassword
        {
            get
            {
                smsApiPassword = smsApiPassword ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiPassword").SettingValue;
                return smsApiPassword;
            }
            set
            {
                smsApiPassword = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiPassword").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string smsApiSenderId;
        public string SmsApiSenderId
        {
            get
            {
                smsApiSenderId = smsApiSenderId ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiSenderId").SettingValue;
                return smsApiSenderId;
            }
            set
            {
                smsApiSenderId = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "SmsApiSenderId").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string tinyPngKey;
        public string TinyPngKey
        {
            get
            {
                tinyPngKey = tinyPngKey ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "TinyPngKeys").SettingValue;
                return tinyPngKey;
            }
            set
            {
                tinyPngKey = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "TinyPngKeys").SettingValue = value;
                context.SaveChanges();
            }
        }

        private string grabberUrl;
        public string GrabberUrl
        {
            get
            {
                grabberUrl = grabberUrl ?? context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "GrabberUrl").SettingValue;
                return grabberUrl;
            }
            set
            {
                grabberUrl = value;
                context.ApplicationSettings.FirstOrDefault(aps => aps.SettingName == "GrabberUrl").SettingValue = value;
                context.SaveChanges();
            }
        }

        public Dictionary<string, bool> TinyPngKeys
        {
            get
            {
                return JsonConvert.DeserializeObject<Dictionary<string, bool>>(TinyPngKey);
            }
            set
            {
                TinyPngKey = JsonConvert.SerializeObject(value);
            }
        }

    }
}