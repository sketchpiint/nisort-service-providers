﻿using System.Collections.Generic;

namespace SP.Services.Core.Management.Configuration
{
    public interface IApplicationSettings
    {
        string ApplicationName { get; set; }
        string FusionApiUrl { get; set; }
        string FusionPassword { get; set; }
        string FusionUserName { get; set; }
        string InvitationLink { get; set; }
        short InvitationLinkExpiryHours { get; set; }
        int MaximumWorkFactor { get; set; }
        int MinimumWorkFactor { get; set; }
        int PasswordLength { get; set; }
        int PasswordSaltLength { get; set; }
        string QueryStringToken { get; set; }
        string ResetPasswordLink { get; set; }
        short ResetPasswordLinkExpiryHours { get; set; }
        short SetPasswordExpiryHours { get; set; }
        string SetPasswordLink { get; set; }
        string SupportEmail { get; set; }
        string SupportMsisdn { get; set; }
        string SystemEmailPassword { get; set; }
        short SystemEmailPort { get; set; }
        string SystemEmailSender { get; set; }
        string SystemEmailServer { get; set; }
        bool? SystemEmailSsl { get; set; }
        string ImagesLocation { get; set; }
        int MaximumImageCount { get; set; }
        int GracePeriodDays { get; set; }
        string SmsApiUrl { get; set; }
        string SmsApiUsername { get; set; }
        string SmsApiPassword { get; set; }
        string SmsApiSenderId { get; set; }
        Dictionary<string, bool> TinyPngKeys { get; set; }
        string TinyPngKey { get; set; }
        string GrabberUrl { get; set; }
    }
}