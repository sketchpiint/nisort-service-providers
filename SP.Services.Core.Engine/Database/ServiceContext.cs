﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using SP.Services.Core.Engine.Entities;

namespace SP.Services.Core.Engine.Database
{
    public class ServiceContext : DbContext
    {
        public ServiceContext(DbContextOptions options) : base(options)
        {
            ContextOptions = options;
            ConnectionString = ((NpgsqlOptionsExtension)options.Extensions.Last()).ConnectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<ProviderService>().HasKey(p => new { p.ProviderId, p.ServiceId });
        }

        public DbContextOptions ContextOptions { get; set; }
        public string ConnectionString { get; set; }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Agency> Agencies { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<SuspensionLog> SuspensionLogs { get; set; }
        public virtual DbSet<TransactionLog> TransactionLogs { get; set; }
        public virtual DbSet<BillingAccount> BillingAccounts { get; set; }
        public virtual DbSet<ProviderService> ProviderServices { get; set; }
        public virtual DbSet<ServiceCategory> ServiceCategories { get; set; }
        public virtual DbSet<NotificationType> NotificationTypes { get; set; }
        public virtual DbSet<SuspensionReason> SuspensionReasons { get; set; }
        public virtual DbSet<SecurityLinkToken> SecurityLinkTokens { get; set; }
        public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public virtual DbSet<SecurityLinkTokenType> SecurityLinkTokenType { get; set; }

    }
}
