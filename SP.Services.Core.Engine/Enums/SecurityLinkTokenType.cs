﻿namespace SP.Services.Core.Engine.Enums
{
    public enum SecurityLinkTokenTypes
    {
        SetPassword = 1,
        ResetPassword,
        Invitation
    }
}
