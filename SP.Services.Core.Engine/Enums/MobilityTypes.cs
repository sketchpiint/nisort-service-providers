﻿namespace SP.Services.Core.Engine.Enums
{
    public enum MobilityTypes
    {
        Mobile = 1,
        FixedLocation,
        Both
    }
}
