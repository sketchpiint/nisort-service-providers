﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SP.Services.Core.Engine.Enums
{
    public enum IdentificationTypes
    {
        NationalId = 1,
        Passport,
        MilitaryId,
        KraPin,

    }
}
