﻿namespace SP.Services.Core.Engine.Enums
{
    public enum SuspensionReasons
    {
        LowAccountBalance = 1,
        Administrative
    }
}
