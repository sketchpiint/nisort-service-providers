﻿namespace SP.Services.Core.Engine.Entities
{
    public class Service
    {
        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public string CategoriesJson { get; set; }
        public string Metadata { get; set; }
    }
}
