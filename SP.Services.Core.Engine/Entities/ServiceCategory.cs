﻿using System.ComponentModel.DataAnnotations;

namespace SP.Services.Core.Engine.Entities
{
    public class ServiceCategory
    {
        [Key]
        public string Name { get; set; }
    }
}
