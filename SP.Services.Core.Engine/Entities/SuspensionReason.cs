﻿namespace SP.Services.Core.Engine.Entities
{
    public class SuspensionReason
    {
        public int SuspensionReasonId { get; set; }
        public string Name { get; set; }
    }
}
