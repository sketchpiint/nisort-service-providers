﻿using System;

namespace SP.Services.Core.Engine.Entities
{
    public class SuspensionLog
    {
        public long SuspensionLogId { get; set; }
        public long ProviderId { get; set; }
        public int SuspensionReasonId { get; set; }
        public string SuspensionDetail { get; set; }
        public DateTime SuspensionDate { get; set; }
        public DateTime? ReinstatedDate { get; set; } = null;
        public string SuspendedBy { get; set; }
        public string ReinstatedBy { get; set; }
        public string ReinstateDetail { get; set; }

        public virtual Provider Provider { get; set; }
        public virtual SuspensionReason SuspensionReason { get; set; }
    }
}
