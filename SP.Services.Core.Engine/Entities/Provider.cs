﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using SP.Services.Core.Engine.Enums;

namespace SP.Services.Core.Engine.Entities
{
    public class Provider
    {
        public long ProviderId { get; set; }
        public long UserId { get; set; }
        public ProviderTypes ProviderType { get; set; } = ProviderTypes.Individual;
        public MobilityTypes MobilityType { get; set; } = MobilityTypes.Mobile;
        public string GeneralLocation { get; set; }
        public string Town { get; set; }
        public string Area { get; set; }
        public string BoundingBoxJson { get; set; }
        public bool Active { get; set; } = true;
        public string Nationality { get; set; }
        public string HairType { get; set; }
        public string HairColor { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string HeightCentimeters { get; set; }
        public string ChestCentimeters { get; set; }
        public string WaistCentimeters { get; set; }
        public string Physique { get; set; }
        public string EyeColor { get; set; }
        public string Bio { get; set; }
        public string MetaDataJson { get; set; }
        public bool Suspended { get; set; } = false;
        public string Languages { get; set; }
        public int Weight { get; set; }
        public string Availability { get; set; }        

        public virtual User User { get; set; }
    }
}
