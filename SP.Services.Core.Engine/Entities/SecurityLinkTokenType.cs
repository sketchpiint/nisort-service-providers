﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SP.Services.Core.Engine.Entities
{
    public class SecurityLinkTokenType
    {
        public int SecurityLinkTokenTypeId { get; set; }
        public string Name { get; set; }
    }
}
