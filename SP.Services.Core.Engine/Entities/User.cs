﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SP.Services.Core.Engine.Entities
{
    public class User
    {
        public long UserId { get; set; }        
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public string Msisdn { get; set; }
        public string Email { get; set; }
        public bool IsProvider { get; set; } = false;
        public bool Active { get; set; } = true;
        public string ProfilePictureUrl { get; set; }
        public string AlbumUrl { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public byte[] Salt { get; set; }
        public byte[] Password { get; set; }
        public int WorkFactor { get; set; } = 20000;
        public bool PasswordSet { get; set; } = false;
    }
}
