﻿namespace SP.Services.Core.Engine.Entities
{

    public class ApplicationSetting
    {
        public int ApplicationSettingId { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
}