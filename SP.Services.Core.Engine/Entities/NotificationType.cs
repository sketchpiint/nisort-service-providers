﻿namespace SP.Services.Core.Engine.Entities
{
    public class NotificationType
    {
        public int NotificationTypeId { get; set; }
        public string Name { get; set; }
    }
}
