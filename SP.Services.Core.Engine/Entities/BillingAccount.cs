﻿using System;

namespace SP.Services.Core.Engine.Entities
{
    public class BillingAccount
    {
        public long BillingAccountId { get; set; }
        public long ProviderId { get; set; }
        public int AccountBalance { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
        public DateTime? PaymentDate { get; set; }
        public DateTime NextPaymentDate { get; set; }
        public bool BillingTimerPaused { get; set; }
        public DateTime? DatePaused { get; set; }
        public bool ResetOnResume { get; set; }
        public string PausedBy { get; set; }

        /// <summary>
        /// The grace period before billing cycle begins after fresh registration or after resume from pause
        /// </summary>
        public int DaysToCountDown { get; set; }

        public Provider Provider { get; set; }
    }
}
