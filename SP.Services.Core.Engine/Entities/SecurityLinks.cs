﻿using System;
using System.Collections.Generic;
using System.Text;
using SP.Services.Core.Engine.Enums;

namespace SP.Services.Core.Engine.Entities
{
    public class SecurityLinkToken
    {
        public long SecurityLinkTokenId { get; set; }
        public string Email { get; set; }
        public Guid LinkToken { get; set; }
        public int SecurityLinkTokenTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool Retrieved { get; set; }

        public SecurityLinkTokenType SecurityLinkTokenType { get; set; }
    }
}
