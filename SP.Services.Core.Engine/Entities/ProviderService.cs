﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SP.Services.Core.Engine.Entities
{
    public class ProviderService
    {
        public long ProviderServiceId { get; set; }
        //[Key, Column(Order =1), ForeignKey("Provider")]
        public long ProviderId { get; set; }

        //[Key, Column(Order = 1), ForeignKey("Service")]
        public int ServiceId { get; set; }
        public bool Active { get; set; } = true;

        public virtual Provider Provider { get; set; }
        public virtual Service Service { get; set; }
    }
}
