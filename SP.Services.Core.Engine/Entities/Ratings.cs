﻿using System;

namespace SP.Services.Core.Engine.Entities
{
    public class Rating
    {
        public long RatingId { get; set; }
        public long ProviderId { get; set; }
        public int RatingValue { get; set; }
        public string Review { get; set; }
        public string ReviewerName { get; set; } //Identify on what job they got that rating

        public string UserIpAddress { get; set; }
        public string RatedByIdentifier { get; set; }
        public DateTime RatingDate { get; set; }

        public void SetIdentifier() => this.RatedByIdentifier = $"{UserIpAddress}-{RatingDate}-{Guid.NewGuid()}";
    }
}
