﻿using System;

namespace SP.Services.Core.Engine.Entities
{
    public class TransactionLog
    {
        public long TransactionLogId { get; set; }
        public long ProviderId { get; set; }
        public long BillingAccountId { get; set; }
        public long Amount { get; set; }
        public long BalanceBefore { get; set; }
        public long BalanceAfter { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ExtraDetail { get; set; }
    }
}
