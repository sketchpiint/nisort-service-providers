﻿namespace SP.Services.Core.Engine.Entities
{
    public class Agency
    {
        public long AgencyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Country { get; set; }
        public string Bio { get; set; }
        public string BoundingBoxJson { get; set; }
        public bool Active { get; set; } = true;
        public bool DisplayProfile { get; set; } = true;
        public string GeneralLocation { get; set; }
        public string Town { get; set; }
        public string Area { get; set; }

        public byte[] Salt { get; set; }
        public byte[] Password { get; set; }
        public int WorkFactor { get; set; } = 20000;
        public bool PasswordSet { get; set; } = false;
    }
}
