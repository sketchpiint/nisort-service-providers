﻿using System;

namespace SP.Services.Core.Engine.Entities
{
    public class Notification
    {
        public long NotificationId { get; set; }
        public long UserId { get; set; }
        public int NotificationTypeId { get; set; }
        public bool Sent { get; set; } = false;
        public bool Retry { get; set; } = true;
        public int RetryCount { get; set; } = 0;
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime DateQueued { get; set; }
        public DateTime? DateSent { get; set; }
        public string Error { get; set; }
        public string Msisdn { get; set; }
        public string Email { get; set; }


        public virtual NotificationType NotificationType { get; set; }
    }
}
