﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SP.Services.Core.Engine.Migrations
{
    public partial class local_01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Agencies",
                columns: table => new
                {
                    AgencyId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Active = table.Column<bool>(nullable: false),
                    Area = table.Column<string>(nullable: true),
                    Bio = table.Column<string>(nullable: true),
                    BoundingBoxJson = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    DisplayProfile = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    GeneralLocation = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Password = table.Column<byte[]>(nullable: true),
                    PasswordSet = table.Column<bool>(nullable: false),
                    Salt = table.Column<byte[]>(nullable: true),
                    Town = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    WorkFactor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agencies", x => x.AgencyId);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationSettings",
                columns: table => new
                {
                    ApplicationSettingId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    SettingName = table.Column<string>(nullable: true),
                    SettingValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationSettings", x => x.ApplicationSettingId);
                });

            migrationBuilder.CreateTable(
                name: "NotificationTypes",
                columns: table => new
                {
                    NotificationTypeId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotificationTypes", x => x.NotificationTypeId);
                });

            migrationBuilder.CreateTable(
                name: "SecurityLinkTokenType",
                columns: table => new
                {
                    SecurityLinkTokenTypeId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecurityLinkTokenType", x => x.SecurityLinkTokenTypeId);
                });

            migrationBuilder.CreateTable(
                name: "ServiceCategories",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceCategories", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    ServiceId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CategoriesJson = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Metadata = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.ServiceId);
                });

            migrationBuilder.CreateTable(
                name: "SuspensionReasons",
                columns: table => new
                {
                    SuspensionReasonId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspensionReasons", x => x.SuspensionReasonId);
                });

            migrationBuilder.CreateTable(
                name: "TransactionLogs",
                columns: table => new
                {
                    TransactionLogId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Amount = table.Column<long>(nullable: false),
                    BalanceAfter = table.Column<long>(nullable: false),
                    BalanceBefore = table.Column<long>(nullable: false),
                    BillingAccountId = table.Column<long>(nullable: false),
                    ExtraDetail = table.Column<string>(nullable: true),
                    ProviderId = table.Column<long>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionLogs", x => x.TransactionLogId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Active = table.Column<bool>(nullable: false),
                    AlbumUrl = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    IsProvider = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    Msisdn = table.Column<string>(nullable: true),
                    OtherName = table.Column<string>(nullable: true),
                    Password = table.Column<byte[]>(nullable: true),
                    PasswordSet = table.Column<bool>(nullable: false),
                    ProfilePictureUrl = table.Column<string>(nullable: true),
                    Salt = table.Column<byte[]>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    WorkFactor = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    NotificationId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateQueued = table.Column<DateTime>(nullable: false),
                    DateSent = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Error = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Msisdn = table.Column<string>(nullable: true),
                    NotificationTypeId = table.Column<int>(nullable: false),
                    Retry = table.Column<bool>(nullable: false),
                    RetryCount = table.Column<int>(nullable: false),
                    Sent = table.Column<bool>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.NotificationId);
                    table.ForeignKey(
                        name: "FK_Notifications_NotificationTypes_NotificationTypeId",
                        column: x => x.NotificationTypeId,
                        principalTable: "NotificationTypes",
                        principalColumn: "NotificationTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SecurityLinkTokens",
                columns: table => new
                {
                    SecurityLinkTokenId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: true),
                    LinkToken = table.Column<Guid>(nullable: false),
                    Retrieved = table.Column<bool>(nullable: false),
                    SecurityLinkTokenTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecurityLinkTokens", x => x.SecurityLinkTokenId);
                    table.ForeignKey(
                        name: "FK_SecurityLinkTokens_SecurityLinkTokenType_SecurityLinkTokenTypeId",
                        column: x => x.SecurityLinkTokenTypeId,
                        principalTable: "SecurityLinkTokenType",
                        principalColumn: "SecurityLinkTokenTypeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Providers",
                columns: table => new
                {
                    ProviderId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Active = table.Column<bool>(nullable: false),
                    Area = table.Column<string>(nullable: true),
                    Availability = table.Column<string>(nullable: true),
                    Bio = table.Column<string>(nullable: true),
                    BoundingBoxJson = table.Column<string>(nullable: true),
                    ChestCentimeters = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    EyeColor = table.Column<string>(nullable: true),
                    GeneralLocation = table.Column<string>(nullable: true),
                    HairColor = table.Column<string>(nullable: true),
                    HairType = table.Column<string>(nullable: true),
                    HeightCentimeters = table.Column<string>(nullable: true),
                    Languages = table.Column<string>(nullable: true),
                    MetaDataJson = table.Column<string>(nullable: true),
                    MobilityType = table.Column<int>(nullable: false),
                    Nationality = table.Column<string>(nullable: true),
                    Physique = table.Column<string>(nullable: true),
                    ProviderType = table.Column<int>(nullable: false),
                    Suspended = table.Column<bool>(nullable: false),
                    Town = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    WaistCentimeters = table.Column<string>(nullable: true),
                    Weight = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Providers", x => x.ProviderId);
                    table.ForeignKey(
                        name: "FK_Providers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BillingAccounts",
                columns: table => new
                {
                    BillingAccountId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AccountBalance = table.Column<int>(nullable: false),
                    BillingTimerPaused = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DatePaused = table.Column<DateTime>(nullable: true),
                    DaysToCountDown = table.Column<int>(nullable: false),
                    NextPaymentDate = table.Column<DateTime>(nullable: false),
                    PausedBy = table.Column<string>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    ProviderId = table.Column<long>(nullable: false),
                    ResetOnResume = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingAccounts", x => x.BillingAccountId);
                    table.ForeignKey(
                        name: "FK_BillingAccounts_Providers_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Providers",
                        principalColumn: "ProviderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProviderServices",
                columns: table => new
                {
                    ProviderServiceId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Active = table.Column<bool>(nullable: false),
                    ProviderId = table.Column<long>(nullable: false),
                    ServiceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderServices", x => x.ProviderServiceId);
                    table.ForeignKey(
                        name: "FK_ProviderServices_Providers_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Providers",
                        principalColumn: "ProviderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProviderServices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "ServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SuspensionLogs",
                columns: table => new
                {
                    SuspensionLogId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ProviderId = table.Column<long>(nullable: false),
                    ReinstateDetail = table.Column<string>(nullable: true),
                    ReinstatedBy = table.Column<string>(nullable: true),
                    ReinstatedDate = table.Column<DateTime>(nullable: true),
                    SuspendedBy = table.Column<string>(nullable: true),
                    SuspensionDate = table.Column<DateTime>(nullable: false),
                    SuspensionDetail = table.Column<string>(nullable: true),
                    SuspensionReasonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuspensionLogs", x => x.SuspensionLogId);
                    table.ForeignKey(
                        name: "FK_SuspensionLogs_Providers_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Providers",
                        principalColumn: "ProviderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuspensionLogs_SuspensionReasons_SuspensionReasonId",
                        column: x => x.SuspensionReasonId,
                        principalTable: "SuspensionReasons",
                        principalColumn: "SuspensionReasonId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BillingAccounts_ProviderId",
                table: "BillingAccounts",
                column: "ProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_NotificationTypeId",
                table: "Notifications",
                column: "NotificationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Providers_UserId",
                table: "Providers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ProviderServices_ProviderId",
                table: "ProviderServices",
                column: "ProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProviderServices_ServiceId",
                table: "ProviderServices",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_SecurityLinkTokens_SecurityLinkTokenTypeId",
                table: "SecurityLinkTokens",
                column: "SecurityLinkTokenTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspensionLogs_ProviderId",
                table: "SuspensionLogs",
                column: "ProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_SuspensionLogs_SuspensionReasonId",
                table: "SuspensionLogs",
                column: "SuspensionReasonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Agencies");

            migrationBuilder.DropTable(
                name: "ApplicationSettings");

            migrationBuilder.DropTable(
                name: "BillingAccounts");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "ProviderServices");

            migrationBuilder.DropTable(
                name: "SecurityLinkTokens");

            migrationBuilder.DropTable(
                name: "ServiceCategories");

            migrationBuilder.DropTable(
                name: "SuspensionLogs");

            migrationBuilder.DropTable(
                name: "TransactionLogs");

            migrationBuilder.DropTable(
                name: "NotificationTypes");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "SecurityLinkTokenType");

            migrationBuilder.DropTable(
                name: "Providers");

            migrationBuilder.DropTable(
                name: "SuspensionReasons");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
