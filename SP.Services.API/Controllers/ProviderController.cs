﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SP.Services.Core.Management;
using SP.Services.Core.Management.Logging;
using SP.Services.API.Models;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.Extensions;
using SP.Services.Core.Management.Helper;
using Exceptionless;
using SP.Services.API.Globals;
using SP.Services.API.Filters;
using SP.Services.Core.Management.CRM;
using SP.Services.API.Auth;
using Microsoft.AspNetCore.Authorization;

namespace SP.Services.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class ProviderController : Controller
    {
        IProviderRepository _providerRepository;
        IMapper _mapper;
        IZoho _zoho;

        public ProviderController(IProviderRepository providerRepository, IMapper mapper, IZoho zoho)
        {
            _providerRepository = providerRepository;
            _mapper = mapper;
            _zoho = zoho;
        }

        [HttpGet]
        public IEnumerable<ProviderModel> GetAll()
        {
            return _mapper.Map<List<ProviderModel>>(_providerRepository.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            var provider = _providerRepository.ValidatedSearch(p => p.ProviderId == id, checkActive: true);
            return new ObjectResult(Mapper.Map<ProviderModel>(provider));
        }

        [HttpGet("GetByUserId/{userId}")]
        public IActionResult GetByUserId(long userId)
        {
            var provider = _providerRepository.ValidatedSearch(p => p.UserId == userId, checkActive: true);

            return new ObjectResult(Mapper.Map<ProviderModel>(provider));
        }

        [HttpGet("GetByUsername/{username}")]
        public IActionResult GetByUsername(string username)
        {
            var provider = _providerRepository.ValidatedSearch(p => p.User.Username == username, checkActive: true);

            return new ObjectResult(Mapper.Map<ProviderModel>(provider));
        }

        [HttpGet("GetByServiceId/{serviceId}")]
        public async Task<IActionResult> GetProviders(int serviceId, int startIndex, int pageSize, string orderBy = "ProviderId DESC")
        {

            var providerData = await _providerRepository.GetProviders(serviceId, startIndex, pageSize, orderBy = "ProviderId DESC");

            var providerModels = Mapper.Map<List<ProviderModel>>(providerData.Providers);
            return new ObjectResult(new { providerModels, providerData.TotalCount });

        }

        [HttpPost("GetByArea")]
        public async Task<IActionResult> GetByArea(string area, int startIndex, int pageSize, string orderBy = "ProviderId DESC")
        {

            var result = await _providerRepository.GetProviders(area, pageSize, startIndex, orderBy);

            var providerModels = Mapper.Map<List<ProviderModel>>(result.data);
            return new ObjectResult(new { Data = providerModels, TotalCount = result.totalCount });

        }

        [HttpPost("GetByTown")]
        public async Task<IActionResult> GetByTown(string town, int startIndex, int pageSize, string orderBy = "ProviderId DESC")
        {
            var result = await _providerRepository.GetProvidersByTown(town, pageSize, startIndex, orderBy);

            var providerModels = Mapper.Map<List<ProviderModel>>(result.data);
            return new ObjectResult(new { Data = providerModels, TotalCount = result.totalCount });
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody]ProviderModel model)
        {
            //if (model.DateOfBirth == null || model.DateOfBirth == default(DateTime))
            model.DateOfBirth = DateTime.Now.AddYears(-model.Age);

            if (model.ChestCentimeters.IsNull())
            {
                var size = model.ChestFeetInches.SplitFeetInches();
                model.ChestCentimeters = ((int)HelperRepository.GetCentimeters(size.feet, size.inches)).ToString();
            }

            if (model.HeightCentimeters.IsNull())
            {
                var size = model.HeightFeetInches.SplitFeetInches();
                model.HeightCentimeters = ((int)HelperRepository.GetCentimeters(size.feet, size.inches)).ToString();
            }

            if (model.WaistCentimeters.IsNull())
            {
                var size = model.WaistFeetInches.SplitFeetInches();
                model.WaistCentimeters = ((int)HelperRepository.GetCentimeters(size.feet, size.inches)).ToString();
            }

            var provider = await _providerRepository.CreateProvider(model.UserId, model.BoundingBoxJson, model.GeneralLocation, model.MobilityType, model.ProviderType, model.ChestCentimeters, model.HeightCentimeters, (DateTime)model.DateOfBirth, model.EyeColor, model.HairColor, model.HairType, model.Nationality, model.Physique, model.WaistCentimeters, model.Languages, model.Area, model.Town, model.Suspended, model.Bio, model.Availability, model.MetaDataJson, model.Weight);

            return new ObjectResult(Mapper.Map<ProviderModel>(provider));

        }

        [HttpPut]
        public async Task<IActionResult> EditProvider([FromBody] ProviderModel model)
        {

            if (model.DateOfBirth == null || model.DateOfBirth == default(DateTime)) model.DateOfBirth = DateTime.Now.AddYears(-model.Age);

            if (model.ChestCentimeters.IsNull(trueIfZero: true))
            {
                var size = model.ChestFeetInches.SplitFeetInches();
                model.ChestCentimeters = ((int)HelperRepository.GetCentimeters(size.feet, size.inches)).ToString();
            }

            if (model.HeightCentimeters.IsNull(trueIfZero: true))
            {
                var size = model.HeightFeetInches.SplitFeetInches();
                model.HeightCentimeters = ((int)HelperRepository.GetCentimeters(size.feet, size.inches)).ToString();
            }

            if (model.WaistCentimeters.IsNull(trueIfZero: true))
            {
                var size = model.WaistFeetInches.SplitFeetInches();
                model.WaistCentimeters = ((int)HelperRepository.GetCentimeters(size.feet, size.inches)).ToString();
            }

            return new ObjectResult(await _providerRepository.EditProvider(_mapper.Map<Provider>(model)));
        }

        [HttpPost("Services/Add")]
        public async Task<IActionResult> AddServices([FromBody]List<ProviderServicesModel> model)
        {
            await _providerRepository.AddProviderService(Mapper.Map<List<ProviderService>>(model));

            return Ok();
        }

        [HttpGet("Services/Get/{id}")]
        public async Task<IActionResult> GetServices(long id)
        {
            var services = await _providerRepository.GetServices(id);

            return new ObjectResult(Mapper.Map<List<ServiceModel>>(services));
        }

        [HttpPost("Services/Remove")]
        public async Task<IActionResult> RemoveService(long providerId, int serviceId)
        {
            await _providerRepository.RemoveProviderService(providerId, serviceId);

            return Ok();
        }

        [HttpGet("GetAllProviderServices")]
        public async Task<IActionResult> GetAllProviderServices(int pageSize, int startIndex, string orderByClause)
        {
            var result = await _providerRepository.GetProviderServices(pageSize, startIndex, orderByClause);

            PagedProviderServiceModel response = new PagedProviderServiceModel
            {
                Data = _mapper.Map<List<ProviderServiceModel>>(result.services),
                TotalCount = result.count
            };

            return new ObjectResult(response);
        }

        [HttpGet("GetAllWithServices")]
        public async Task<IActionResult> GetProvidersWithServices(int pageSize, int startIndex, string orderByClause)
        {
            var result = await _providerRepository.GetProvidersWithServices(pageSize, startIndex, orderByClause);

            PagedProviderWithServicesModel response = new PagedProviderWithServicesModel
            {
                TotalCount = result.count,
                Data = new List<ProviderWithServicesModel>()
            };

            result.providerServices.ForEach(s => response.Data.Add(new ProviderWithServicesModel { ProviderModel = _mapper.Map<ProviderModel>(s.provider), Services = _mapper.Map<List<ServiceModel>>(s.services) }));


            return new ObjectResult(response);
        }

        [HttpGet("GetRegisteredAreas")]
        public IActionResult GetRegisteredAreas()
        {
            return new ObjectResult(_providerRepository.GetRegisteredAreas());
        }

        [HttpGet("GetRegisteredTowns")]
        public IActionResult GetRegisteredTowns()
        {
            return new ObjectResult(_providerRepository.GetRegisteredTowns());
        }

        [HttpGet("Search")]
        public IActionResult Search(SearchModel model, int startIndex, int pageSize, string orderByClause, DateTime? addedAfter = null, DateTime? addedBefore = null, string username = "", bool? suspended = false, bool admin = false)
        {
            if (orderByClause.IsNull()) orderByClause = "ProviderId";
            var result = _providerRepository.Filter(model.Locations, model.MinimumAge, model.MaximumAge, model.Name, model.ServiceIds, model.MinimumHeightCm, model.MaximumHeightCm, pageSize, startIndex, orderByClause, addedAfter, addedBefore, username, suspended, admin);

            return new ObjectResult(new
            {
                TotalCount = result.TotalCount,
                Providers = _mapper.Map<List<ProviderModel>>(result.Providers)
            });
        }

        [HttpGet("AddLead")]
        public async Task AddLead(long userId, long providerId, string username, string msisdn, string name, string location)
        {
            await _zoho.AddAndConvertLead(userId, providerId, username, msisdn, name, location);
        }

        /// <summary>
        /// Get all providers who have at least one safe image. Safe here indicates non-explicit.
        /// </summary>
        /// <param name="providerCount">Number of items to return - think of it as the page size</param>
        /// <returns>A list of providers</returns>
        [HttpGet("GetWithSafeImages"), ProducesResponseType(typeof(List<ProviderModel>), 200)]
        public async Task<IActionResult> GetWithSafeImages(int providerCount)
        {
            return new ObjectResult(_mapper.Map<List<ProviderModel>>(await _providerRepository.GetWithNonExplicitImages(providerCount)));
        }
    }
}
