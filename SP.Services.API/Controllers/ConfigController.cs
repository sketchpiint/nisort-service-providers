﻿using System;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SP.Services.API.Models;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.Logging;
using Exceptionless;
using SP.Services.API.Globals;
using SP.Services.API.Filters;
using SP.Services.API.Auth;
using Microsoft.AspNetCore.Authorization;

namespace SP.Services.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class ConfigController : Controller
    {
        IApplicationSettings _appSettings;
        IMapper _mapper;

        public ConfigController(IApplicationSettings appSettings, IMapper mapper)
        {
            this._appSettings = appSettings;
            this._mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            ConfigModel model = new ConfigModel
            {
                ApplicationName = _appSettings.ApplicationName,
                MaximumImageCount = _appSettings.MaximumImageCount,
                PasswordLength = _appSettings.PasswordLength,
                SupportEmail = _appSettings.SupportEmail,
                SupportMsisdn = _appSettings.SupportMsisdn
            };

            return new ObjectResult(model);
        }
    }
}
