﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SP.Services.API.Filters;
using SP.Services.API.Models;
using SP.Services.Core.Engine.Enums;
using SP.Services.Core.Management;
using SP.Services.Core.Management.Logging;
using SP.Services.API.Auth;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SP.Services.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class NotificationsController : Controller
    {
        INotificationRepository _notificationsRepository;
        IMapper _mapper;

        public NotificationsController(INotificationRepository notificationRepository, IMapper mapper)
        {
            _notificationsRepository = notificationRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(_mapper.Map<List<NotificationModel>>(_notificationsRepository.Search(n => !n.Sent)));
        }

        [HttpGet("GetByStatus")]
        public IActionResult GetByStatus(bool sent)
        {
            return new ObjectResult(_mapper.Map<List<NotificationModel>>(_notificationsRepository.Search(n => n.Sent == sent && n.Retry)));
        }


        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            return new ObjectResult(_mapper.Map<NotificationModel>(_notificationsRepository.ValidatedSearch(n => n.NotificationId == id)));
        }

        [HttpGet("Send")]
        public IActionResult SendNotifications()
        {
            return new ObjectResult(_notificationsRepository.SendNotifications());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NotificationModel model)
        {
            return new ObjectResult(await _notificationsRepository.QueueNotification(model.UserId, model.NotificationType, model.Message, model.Subject, model.Msisdn, model.Email));

        }
    }
}
