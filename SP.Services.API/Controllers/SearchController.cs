﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exceptionless;
using Microsoft.AspNetCore.Mvc;
using SP.Services.API.Filters;
using SP.Services.API.Globals;
using SP.Services.Core.Management;
using SP.Services.Core.Management.Logging;
using SP.Services.API.Auth;
using Microsoft.AspNetCore.Authorization;

namespace SP.Services.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class SearchController : Controller
    {
        ISearchRepository _searchRepository;
        public SearchController(ISearchRepository searchRepository)
        {
            _searchRepository = searchRepository;
        }

        // GET: api/values
        [HttpGet("Proxy/{query}")]
        public async Task<IActionResult> Get(string query)
        {
            return new ObjectResult(await _searchRepository.Search(query));
        }
    }
}
