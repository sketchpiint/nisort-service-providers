﻿using System;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SP.Services.API.Models;
using SP.Services.Core.Management;
using SP.Services.Core.Engine.Entities;
using SP.Services.Core.Management.Logging;

using Exceptionless;
using Microsoft.Extensions.Configuration;
using SP.Services.API.Globals;
using SP.Services.API.Filters;
using SP.Services.API.Auth;
using System.Security.Claims;
using System.Net;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using SP.Services.API.Extensions;

namespace SP.Services.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class AuthController : Controller
    {
        IMapper _mapper;
        IJwtFactory _jwtFactory;
        JwtIssuerOptions _jwtOptions;
        IUserRepository _userRepository;
        private readonly string _clientId;

        public AuthController(IMapper mapper, IUserRepository userRepository, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions, IHttpContextAccessor contextAccessor)
        {
            _mapper = mapper;
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;
            _userRepository = userRepository;
            _clientId = contextAccessor.GetClientId();
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {           
            User user = await _userRepository.UserLogin(model.Identifier, model.PasswordClaim);

            // This scenario is already handled in the repository. This is just a failsafe
            if (user == null) throw new CustomException("Invalid username or password", HttpStatusCode.Unauthorized);

            ClaimsIdentity claimsIdentity = await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(model.Identifier, user.UserId.ToString(), user.Email, user.Msisdn, _clientId));

            var jwt = await Tokens.GenerateJwt(claimsIdentity, _jwtFactory, model.Identifier, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

            return new ObjectResult(new { User = _mapper.Map<UserModel>(user), Token = jwt });
        }

        [AllowAnonymous]
        [HttpPost("GetAccessToken")]
        public async Task<IActionResult> GetAccessToken([FromBody]ClientCredentialsModel model)
        {
            // ToDo: Will simulate for now as there's no db provision
            var clients = new[] {
                new
                {
                    ClientId = "SP.Services.web",
                    ClientSecret = "a7322ee4-2733-4181-a7e3-8fadc8495973"
                },
                new
                {
                    ClientId = "SP.Services.admin",
                    ClientSecret = "4c33eca1-eb04-468c-9a7b-3e2ebdb1e0e3"
                }}.ToList();

            var client = clients.FirstOrDefault(c => c.ClientId == model.ClientId && c.ClientSecret == model.ClientSecret);

            if (client == null) throw new CustomException("Invalid ClientId or ClientSecret", HttpStatusCode.Unauthorized);            

            ClaimsIdentity claimsIdentity = await Task.FromResult(_jwtFactory.GenerateClientClaimsIdentity(client.ClientId));

            var jwt = await Tokens.GenerateJwt(claimsIdentity, _jwtFactory, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

            return new ObjectResult (new { Client = new { ClientId = client.ClientId }, Token = jwt });
        }

        [HttpPost("ValidateToken")]
        public async Task<IActionResult> ValidateToken([FromBody]TokenModel model)
        {
            return new ObjectResult(await _userRepository.ValidateSecurityToken(model.Token, model.Type));
        }

        [HttpPost("SetPassword")]
        public async Task<IActionResult> SetPassword([FromBody]SetPasswordModel model)
        {
            return new ObjectResult(await _userRepository.SetUserPasswordAsync(model.SecurityToken, model.Password));
        }

        [HttpPost("RecoverPassword/{identifier}")]
        public async Task<IActionResult> RecoverPassword(string identifier, bool administration = false)
        {
            var result = await _userRepository.RecoverPasswordAsync(identifier, administration);
            return new ObjectResult(new { success = result.success, token = result.token });
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody]SetPasswordModel model)
        {
            return new ObjectResult(await _userRepository.SetUserPasswordAsync(model.SecurityToken, model.Password, reset: true));
        }
    }
}
