﻿using System;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

using SP.Services.API.Models;
using SP.Services.Core.Management;
using SP.Services.Core.Management.Logging;
using SP.Services.Core.Engine.Entities;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Exceptionless;
using SP.Services.API.Globals;
using SP.Services.API.Filters;
using Microsoft.AspNetCore.Authorization;
using SP.Services.API.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SP.Services.API.Auth;

namespace SP.Services.API.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilterAttribute]
    public class UserController : Controller
    {
        IUserRepository _userRepository;
        IMapper _mapper;
        ILogger _logger;
        IHostingEnvironment _hostingEnvironment;
        private readonly CurrentUser _currentUser;        
        public UserController(IUserRepository userRepository, IMapper mapper, IHostingEnvironment hostingEnvironment, IHttpContextAccessor contextAccessor, ILoggerFactory loggerFactory)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _logger = loggerFactory.CreateLogger<UserController>();
            _currentUser = contextAccessor.CurrentUser();            
        }

        [HttpGet]
        public IEnumerable<UserModel> GetAll()
        {
            //string clientId = HttpContext.Items["clientId"].ToString();

            //_logger.LogInformation("Current User: \n" + JsonConvert.SerializeObject(_currentUser, Formatting.Indented));
            return _mapper.Map<List<UserModel>>(_userRepository.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            var user = _userRepository.ValidatedSearch(u => u.UserId == id, checkActive: true);

            return new ObjectResult(Mapper.Map<UserModel>(user));
        }

        [HttpGet("GetByUsername/{Username}")]
        public IActionResult GetByUsername(string username)
        {
            var user = _userRepository.ValidatedSearch(u => u.Username == username, checkActive: true);

            return new ObjectResult(Mapper.Map<UserModel>(user));
        }

        [HttpGet("GetByEmail/{Email}")]
        public IActionResult GetByEmail(string Email)
        {
            var user = _userRepository.ValidatedSearch(u => u.Email == Email, checkActive: true);

            return new ObjectResult(Mapper.Map<UserModel>(user));
        }

        [HttpGet("GetByMsisdn/{phoneNumber}")]
        public IActionResult GetByMsisdn(string phoneNumber)
        {
            var user = _userRepository.ValidatedSearch(u => u.Msisdn == phoneNumber, checkActive: true);

            return new ObjectResult(Mapper.Map<UserModel>(user));
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody]UserModel model)
        {
            var user = await _userRepository.CreateUserAsync(model.FirstName, model.LastName, model.OtherName, model.Msisdn, model.Username, model.Email);

            return new ObjectResult(new { User = Mapper.Map<UserModel>(user.user), Token = user.token });
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody]UserModel model)
        {
            return new ObjectResult(await _userRepository.EditUser(_mapper.Map<User>(model)));
        }

        [HttpPost("UploadImages")]
        public async Task<IActionResult> UploadImages(IFormFile file, ImageModel model)
        {
            if (file == null) return BadRequest("File cannot be null");

            if (file.Length == 0) return BadRequest("File cannot be empty");

            using (Stream stream = file.OpenReadStream())
            {
                using (var binaryReader = new BinaryReader(stream))
                {
                    var fileContent = binaryReader.ReadBytes((int)file.Length);
                    return new ObjectResult(await _userRepository.AddImage(fileContent, file.FileName, file.ContentType, model.IsProfilePicture, model.UserId, _hostingEnvironment.ContentRootPath));
                }
            }
        }

        [HttpGet("GetImages/{userId}")]
        public async Task<IActionResult> GetImages(long userId, bool? safeOnly = null)
        {
            return new ObjectResult(await Task.Run(() => _userRepository.GetUserImageNames(userId, safeOnly)));
        }

        [HttpDelete("DeleteImage")]
        public async Task<IActionResult> DeleteImage(long userId, string imageName)
        {
            return new ObjectResult(await Task.Run(() => _userRepository.DeleteUserImage(userId, imageName)));
        }

        [HttpPost("SendFeedback")]
        public IActionResult SendFeedback([FromBody]FeedbackModel model)
        {
            _userRepository.SendFeedback(model.Email, model.FullName, model.Msisdn, model.Message);
            return Ok();
        }

        [HttpPut("EditImageName")]
        public IActionResult EditImageName(long userId, string oldName, string newName)
        {
            return Ok(_userRepository.EditImageName(userId, oldName, newName));
        }

        [HttpPatch("MarkImageExplicit")]
        public void MarkImageExplicit(long userId, string imageName)
        {
            _userRepository.MarkImageExplicit(userId, imageName);
        }

        [HttpPatch("MarkImageSafe")]
        public void MarkImageSafe(long userId, string imageName)
        {
            _userRepository.MarkImageSafe(userId, imageName);
        }

        [HttpGet("UpdateDates")]
        public IActionResult UpdateDates()
        {
            //_userRepository.UpdateDateCreated();
            return Ok();
        }
    }
}
