﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SP.Services.API.Filters;
using SP.Services.Core.Management;
using SP.Services.API.Auth;

namespace SP.Services.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class BillingController : Controller
    {
        IBillingAccountRepository _billingRepository;

        public BillingController(IBillingAccountRepository billingRepository)
        {
            _billingRepository = billingRepository;
        }

        [HttpPost("PauseBillingCycle/{providerId}/{intervalDays}/{pausedBy}")]
        public async Task<IActionResult> PauseBillingCycle(long providerId, int intervalDays, string pausedBy)
        {
            await _billingRepository.PauseProviderBillingCycle(providerId, intervalDays, pausedBy);
            return Ok();
        }

        [HttpGet("GetDaysToNextPayment/{providerId}")]
        public IActionResult GetDaysToNextPayment(long providerId)
        {
            return Ok(_billingRepository.GetDaysToNextPayment(providerId));
        }
    }
}
