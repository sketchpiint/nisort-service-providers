using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SP.Services.Core.Management;
using AutoMapper;
using SP.Services.API.Models;
using SP.Services.Core.Management.Logging;
using SP.Services.Core.Engine.Entities;
using Exceptionless;
using SP.Services.API.Globals;
using SP.Services.API.Filters;
using SP.Services.API.Auth;
using Microsoft.AspNetCore.Authorization;

namespace SP.Services.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Service")]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ApiAccess)]
    [Authorize(Policy = Constants.Strings.JwtPolicies.ClientAccess)]
    [ModelStateValidationFilter, CustomExceptionFilter]
    public class ServiceController : Controller
    {
        IServiceRepository _serviceRepository;
        IMapper _mapper;

        public ServiceController(IServiceRepository serviceRepository, IMapper mapper)
        {
            _serviceRepository = serviceRepository;
            _mapper = mapper;
        }

        // GET: api/Service
        [HttpGet]
        public IEnumerable<ServiceModel> Get()
        {
            return Mapper.Map<List<ServiceModel>>(_serviceRepository.GetAll());
        }

        // GET: api/Service/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var service = _serviceRepository.ValidatedSearch(s => s.ServiceId == id, checkActive: true);

            return new ObjectResult(Mapper.Map<ServiceModel>(service));
        }

        [HttpGet("search/{name}")]
        public IActionResult Search(string name)
        {

            name = System.Net.WebUtility.UrlDecode(name).Replace(",,,", "/");

            var service = _serviceRepository.ValidatedSearch(s => s.Name == name, checkActive: true);

            return new ObjectResult(Mapper.Map<ServiceModel>(service));
        }
    }
}
