﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace SP.Services.API.Auth
{
    public class AuthorizationPolicyProvider : DefaultAuthorizationPolicyProvider
    {
        private readonly IConfiguration _configuration;
        private readonly JwtIssuerOptions _jwtOptions;

        public AuthorizationPolicyProvider(IOptions<AuthorizationOptions> options, IOptions<JwtIssuerOptions> jwtOptions, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
            _jwtOptions = jwtOptions.Value;
        }

        public override async Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            // Get statically declared policies first
            var policy = await base.GetPolicyAsync(policyName);

            if (policy != null) return policy;

            policy = new AuthorizationPolicyBuilder()
                .AddRequirements(new HasScopeRequirement(policyName, _jwtOptions.Issuer))
                .Build();

            return policy;
        }
    }
}
