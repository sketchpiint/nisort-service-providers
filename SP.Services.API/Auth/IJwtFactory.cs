﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace SP.Services.API.Auth
{
    public interface IJwtFactory
    {
        ClaimsIdentity GenerateClientClaimsIdentity(string clientId);
        ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string email, string msisdn, string clientId);
        Task<string> GenerateEncodedToken(ClaimsIdentity identity);
        Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity);
    }
}