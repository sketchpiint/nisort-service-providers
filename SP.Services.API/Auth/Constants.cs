﻿namespace SP.Services.API.Auth
{
    public static class Constants
    {
        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Rol = "rol", Id = "id", Email = "email", Msisdn = "msisdn", ClientId = "client_id";
            }

            public static class JwtClaims
            {
                public const string ApiAccess = "api_access", AdminAccess =  "admin_access", ClientAccess = "app_access";
                
            }

            public static class JwtPolicies
            {
                public const string ApiAccess = "ApiUser", AdminAccess = "AdminUser", ClientAccess = "ClientAccess";
            }
        }
    }
}
