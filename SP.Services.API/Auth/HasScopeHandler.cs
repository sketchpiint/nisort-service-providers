﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace SP.Services.API.Auth
{
    /// <summary>
    /// Checks if the scope claim of the token contains the required claim while also
    /// ensuring the scope claim was issued by the specified issuer
    /// </summary>
    public class HasScopeHandler: AuthorizationHandler<HasScopeRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasScopeRequirement requirement)
        {
            // if the user does not have the scope claim, return
            if (!context.User.HasClaim(c => c.Type == "scope" && c.Issuer != requirement.Issuer)) return Task.CompletedTask;

            //The scopes in the scope claim string should always be space seperated
            var scopes = context.User.FindFirst(c => c.Type == "scope" && c.Issuer == requirement.Issuer).Value.Split(' ');

            // succeed if the scope array contains the required scope
            if (scopes.Any(s => s == requirement.Scope)) context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
