﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SP.Services.API.Models;
using SP.Services.Core.Engine.Entities;
//using SP.Services.Engine.Entities;
//using SP.Services.Management;
//using SP.Services.Web.Models;

namespace SP.Services.Web.AutoMapper
{
    /*public class AppAddedByUsernameResolver : IValueResolver<Application, ApplicationModel, string>
    {
        IUserRepository _userRepository = null;
        public AppAddedByUsernameResolver(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public string Resolve(Application source, ApplicationModel destination, string destMember, ResolutionContext context)
        {
            User user = _userRepository.Find(source.AddedByUserId);
            return user.Username;
        }
    }

    public class AppAddedByUserNameResolver : IValueResolver<Application, ApplicationModel, string>
    {
        IUserRepository _userRepository = null;
        public AppAddedByUserNameResolver(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public string Resolve(Application source, ApplicationModel destination, string destMember, ResolutionContext context)
        {
            User user = _userRepository.Find(source.AddedByUserId);
            return $"{user.FirstName} {user.LastName}";
        }
    }
    */

    public class DobToAgeResolver : IValueResolver<Provider, ProviderModel, int>
    {
        public int Resolve(Provider source, ProviderModel destination, int destMember, ResolutionContext context)
        {
            int age = DateTime.Now.Year - source.DateOfBirth.Year;

            if (DateTime.Now.DayOfYear < source.DateOfBirth.DayOfYear) age--;

            return age;
        }
    }
}
