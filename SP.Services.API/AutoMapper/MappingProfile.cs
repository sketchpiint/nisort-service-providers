﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SP.Services.Core.Engine.Entities;
using SP.Services.API.Models;
using SP.Services.Core.Management.Helper;
using System.IO;
using SP.Services.Core.Management.Extensions;

namespace SP.Services.Web.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            GetMappings();
        }

        private void GetMappings()
        {
            CreateMap<User, UserModel>()
                .ForMember(dest => dest.ProfilePicName, opt => opt
                .ResolveUsing(u =>
                {
                    if (u.ProfilePictureUrl.IsNull()) return null;
                    //string path = Path.GetFileName($"{u.ProfilePictureUrl}{u.UserId}-profile.png");
                    return Path.GetFileName($"{u.ProfilePictureUrl}{u.UserId}-profile.jpg");
                    //return path;
                }));
            CreateMap<UserModel, User>();

            CreateMap<Service, ServiceModel>();
            CreateMap<ServiceModel, Service>();

            CreateMap<Provider, ProviderModel>()
                .ForMember(dest => dest.Age, opt => opt.ResolveUsing(p =>
                {
                    int age = DateTime.Now.Year - p.DateOfBirth.Year;
                    if (DateTime.Now.DayOfYear < p.DateOfBirth.DayOfYear) age--;
                    return age;
                }))
                //.ForMember(dest => dest.ChestFeetInches, opt => opt.ResolveUsing(p =>
                //{
                //    var result = HelperRepository.GetFeetInches(Double.Parse(p.ChestCentimeters));
                //    return $"{result.feet}'{result.inches}";
                //}))
                .ForMember(dest => dest.HeightFeetInches, opt => opt.ResolveUsing(p =>
                {
                    var result = HelperRepository.GetFeetInches(Double.Parse(p.HeightCentimeters));
                    return $"{result.feet}'{result.inches}";
                }))
                .ForMember(dest => dest.WaistFeetInches, opt => opt.ResolveUsing(p =>
                {
                    var result = HelperRepository.GetFeetInches(Double.Parse(p.WaistCentimeters));
                    return $"{result.feet}'{result.inches}";
                }));

            CreateMap<ProviderModel, Provider>();

            CreateMap<ProviderService, ProviderServicesModel>();
            CreateMap<ProviderServicesModel, ProviderService>();

            CreateMap<ProviderService, ProviderServiceModel>();
            CreateMap<ProviderServiceModel, ProviderService>();

            CreateMap<ApplicationSetting, ConfigModel>();
        }
    }
}
