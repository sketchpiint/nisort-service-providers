﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.AspNetCore.Http;
using SP.Services.API.Models;
using SP.Services.API.Auth;
using SP.Services.Core.Management.Extensions;

namespace SP.Services.API.Extensions
{
    public static class IHttpContextAccessorExtension
    {
        public static CurrentUser CurrentUser(this IHttpContextAccessor contextAccessor)
        {
            string clientId = contextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == Constants.Strings.JwtClaimIdentifiers.ClientId)?.Value;

            if (!clientId.IsNull()) return new CurrentUser();

            string username = contextAccessor?.HttpContext?.User?.Claims.Single(c=>c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;
            string jti = contextAccessor?.HttpContext?.User?.FindFirst(JwtRegisteredClaimNames.Jti)?.Value;
            string audience = contextAccessor?.HttpContext?.User?.FindFirst(JwtRegisteredClaimNames.Aud)?.Value;
            string userIdString = contextAccessor?.HttpContext?.User?.Claims?.Single(c => c.Type == "id").Value;
            string email = contextAccessor?.HttpContext?.User?.Claims?.Single(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")?.Value;
            string msisdn = contextAccessor?.HttpContext?.User?.Claims?.Single(c => c.Type == "msisdn")?.Value;

            long.TryParse(userIdString, out long userId);

            return new CurrentUser { UserId = userId, Username = username, Email = email, Msisdn = msisdn };
        }

        public static string GetClientId(this IHttpContextAccessor contextAccessor)
        {
            return contextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == Constants.Strings.JwtClaimIdentifiers.ClientId)?.Value;
        }
    }
}
