﻿namespace SP.Services.API.Models
{
    /// <summary>
    /// A container for API client application credentials
    /// </summary>
    public class ClientCredentialsModel
    {
        /// <summary>
        /// API Client Id. This will normally be assigned by an administrator. 
        /// A later implementation can allow it to be created on a dashboard.
        /// </summary>
        public string  ClientId { get; set; }

        /// <summary>
        /// API client secret. This will be assigned by an administrator.
        /// A later implementation can allow it to be created on a dashboard.
        /// </summary>
        public string ClientSecret { get; set; }
    }
}
