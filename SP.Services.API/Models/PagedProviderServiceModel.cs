﻿using System.Collections.Generic;

namespace SP.Services.API.Models
{
    public class PagedProviderServiceModel
    {
        public List<ProviderServiceModel> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
