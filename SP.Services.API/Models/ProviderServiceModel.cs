﻿using System;
using System.Linq;

namespace SP.Services.API.Models
{
    public class ProviderServiceModel
    {
        public ProviderModel Provider { get; set; }
        public ServiceModel Service { get; set; }
    }
}
