﻿using System.Collections.Generic;

namespace SP.Services.API.Models
{
    public class ProviderServicesModel
    {
        public long ProviderId { get; set; }
        public int ServiceId { get; set; }
    }
}
