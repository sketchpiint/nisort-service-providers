﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SP.Services.Core.Engine.Enums;

namespace SP.Services.API.Models
{
    public class ProviderModel
    {
        public long ProviderId { get; set; }
        public long UserId { get; set; }

        //[Required]
        public ProviderTypes ProviderType { get; set; }

        //[Required]
        public MobilityTypes MobilityType { get; set; }

        public string Town { get; set; }

        public string Area { get; set; }
        public string Bio { get; set; }

        string location = "";

        [Required(ErrorMessage ="The general location of a provider must always be passed. Prefer the format 'Town, Area'")]
        public string GeneralLocation
        {
            get { return location; }
            set
            {
                location = value;
                string[] locationInfo = location.Split(',', StringSplitOptions.RemoveEmptyEntries);
                if (locationInfo.Count() > 0)
                {
                    TrimmedLocation = Area = locationInfo[0];
                    Town = locationInfo.Count() == 1 ? "" : locationInfo[1];
                }
            }
        }

        public string TrimmedLocation { get; set; }

        /// <summary>
        /// A Json of 
        /// </summary>
        [Required(ErrorMessage ="The bounding box of the provider's area of operation must be provided.")]
        public string BoundingBoxJson { get; set; }

        [Required(ErrorMessage ="The nationality of the provider is required.")]
        public string Nationality { get; set; }

        public string HairType { get; set; }
        public string HairColor { get; set; }
        public int Weight { get; set; }
        public string Availability { get; set; }

        /// <summary>
        /// Will be derived from the Age. Always pass 'null' when POSTing.
        /// </summary>
        public DateTime? DateOfBirth { get; set; } = default(DateTime);
        
        [Required(ErrorMessage ="The provider's age must be provided")]
        [Range(minimum: 18, maximum: 100, ErrorMessage = "Sorry, you must be between 18 and 100 years old")]
        public int Age { get; set; }

        public string HeightCentimeters { get; set; }
        public string HeightFeetInches { get; set; }
        public string ChestCentimeters { get; set; }
        public string ChestFeetInches { get; set; }
        public string WaistCentimeters { get; set; }
        public string WaistFeetInches { get; set; }
        public string Languages { get; set; }
        public string Physique { get; set; }
        public string EyeColor { get; set; }
        public bool Suspended { get; set; } = false;
        public bool Active { get; set; } = true;
        public string MetaDataJson { get; set; }

        public UserModel User { get; set; }
    }
}
