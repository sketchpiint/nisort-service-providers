﻿using System;
using System.ComponentModel.DataAnnotations;
using SP.Services.Core.Engine.Enums;

namespace SP.Services.API.Models
{
    public class NotificationModel
    {
        public long NotificationId { get; set; }
        public long UserId { get; set; }

        [Required]
        public int NotificationTypeId { get; set; }
        public NotificationTypes NotificationType { get; set; }
        public bool Sent { get; set; } = false;

        [Required]
        public string Message { get; set; }
        public string Subject { get; set; }
        public DateTime DateQueued { get; set; }
        public DateTime? DateSent { get; set; }
        public string Error { get; set; }        
        public string Msisdn { get; set; }
        public string Email { get; set; }
    }
}
