﻿using System.Collections.Generic;

namespace SP.Services.API.Models
{
    public class PagedProviderWithServicesModel
    {
        public List<ProviderWithServicesModel> Data { get; set; }
        public int TotalCount { get; set; }
    }
}
