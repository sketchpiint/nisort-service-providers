﻿namespace SP.Services.API.Models
{
    public class CurrentUser
    {
        public long UserId { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Msisdn { get; set; }
    }
}
