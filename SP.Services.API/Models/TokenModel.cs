﻿using SP.Services.Core.Engine.Enums;

namespace SP.Services.API.Models
{
    public class TokenModel
    {
        public string Token { get; set; }
        public SecurityLinkTokenTypes Type { get; set; }
    }
}
