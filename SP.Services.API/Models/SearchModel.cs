﻿using System.Collections.Generic;

namespace SP.Services.API.Models
{
    public class SearchModel
    {
        public List<string> Locations { get; set; }
        public List<int> ServiceIds { get; set; }
        public string Name { get; set; }
        public int MinimumAge { get; set; } = 18;
        public int MaximumAge { get; set; } = 100;
        public int MinimumHeightCm { get; set; }
        public int MaximumHeightCm { get; set; }
    }
}
