﻿namespace SP.Services.API.Models
{
    public class ConfigModel
    {
        public int PasswordLength { get; set; }
        public string ApplicationName { get; set; }
        public string SupportMsisdn { get; set; }
        public string SupportEmail { get; set; }
        public int MaximumImageCount { get; set; }
    }
}
