﻿using System.Collections.Generic;

namespace SP.Services.API.Models
{
    public class ProviderWithServicesModel
    {
        public ProviderModel ProviderModel { get; set; }
        public List<ServiceModel> Services { get; set; }
    }
}
