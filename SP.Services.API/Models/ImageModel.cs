﻿namespace SP.Services.API.Models
{
    public class ImageModel
    {
        public bool IsProfilePicture { get; set; }
        public long UserId { get; set; }
    }
}
