﻿using System.ComponentModel.DataAnnotations;

namespace SP.Services.API.Models
{
    /// <summary>
    /// A container to transfer credentials for login purposes
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Field used to identify the user. Can be their username, email or phone number
        /// </summary>
        [Required(ErrorMessage ="The identifier, email, phone number or username, must be provided")]
        public string Identifier { get; set; }

        /// <summary>
        /// The password passed in by the user.
        /// </summary>
        [Required(ErrorMessage = "The password is required")]
        public string PasswordClaim { get; set; }

        /// <summary>
        /// Currently not in use. Will be used to provide a safer transportation of password over the wire.
        /// </summary>
        public string PasswordHash { get; set; }
    }
}
