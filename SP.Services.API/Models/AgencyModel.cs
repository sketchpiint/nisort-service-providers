﻿namespace SP.Services.API.Models
{
    public class AgencyModel
    {
        public long AgencyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Country { get; set; }
        public string Bio { get; set; }
        public string BoundingBoxJson { get; set; }
        public bool Active { get; set; } = true;
        public bool DisplayProfile { get; set; } = true;
        public string GeneralLocation { get; set; }
        public string Town { get; set; }
        public string Area { get; set; }
    }
}
