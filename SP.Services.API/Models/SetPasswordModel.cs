﻿namespace SP.Services.API.Models
{
    public class SetPasswordModel
    {
        public string SecurityToken { get; set; }
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public bool Reset { get; set; } = false;
    }
}
