﻿namespace SP.Services.API.Models
{
    public class ServiceModel
    {
        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public string CategoriesJson { get; set; }
    }
}
