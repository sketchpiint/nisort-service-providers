﻿namespace SP.Services.API.Models
{
    public class FeedbackModel
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Msisdn { get; set; }
        public string Message { get; set; }
    }
}
