﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SP.Services.API.Models
{
    public class UserModel
    {
        public long UserId { get; set; }

        [Required]
        public string Username { get; set; } // Make mandatory for providers in a bid to ease link sharing

        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }

        [Required]
        public string Msisdn { get; set; }

        [Required]
        public string Email { get; set; }
        public string ProfilePicName { get; set; }
        public bool IsProvider { get; set; } = false;
        public bool Active { get; set; } = true;
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
