﻿using System;
using System.IO;
using System.Linq;
using AutoMapper;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using SP.Services.Core.Engine.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.DependencyInjection;

using SP.Services.Core.Management;
using SP.Services.Core.Management.Helper;
using SP.Services.Core.Management.Extensions;
using SP.Services.Core.Management.Configuration;
using SP.Services.Core.Management.Communication;
using Exceptionless;
using Exceptionless.Dependency;
using Microsoft.Net.Http.Headers;
using SP.Services.API.Auth;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using SP.Services.Core.Management.CRM;
using SP.Services.API.Filters;
using Microsoft.AspNetCore.Authorization;

namespace SP.Services.API
{
    public class Startup
    {
        //ToDO: THIS HAS TO BE MOVED TO A SAFER LOCATION
        private const string secretKey = "079FEC4B7F5942598748F3496ABC5B7E";
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

        string swaggerDescription = "";
        string swaggerApiName = "";
        string apiVersion = "";

        string connectionString;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            connectionString = GetConnectionString();
            GetSwaggerInfo();
        }

        public IConfiguration Configuration { get; }

        public void GetSwaggerInfo()
        {
            swaggerApiName = Configuration["Documentation:ApiName"];
            swaggerDescription = Configuration["Documentation:Description"];
            apiVersion = Configuration["Documentation:ApiVersion"];
        }

        public string GetConnectionString()
        {
            if (bool.Parse(Configuration["Env:Production"])) return Configuration.GetConnectionString("ServicesContextPgProduction");
            if (bool.Parse(Configuration["Env:Staging"])) return Configuration.GetConnectionString("ServicesContextPgStaging");
            return Configuration.GetConnectionString("ServicesContextPgLocal");
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var securityMigrationsAssembly = typeof(ServiceContext).GetTypeInfo().Assembly.GetName().Name;

            // Add framework services.
            services.AddMvc().AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddCors();
            services.AddAutoMapper();
            services.AddMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.Name = ".SP.Services.API";
            });

            services.AddSwaggerGen(c =>
            {

                c.SwaggerDoc("v1", new Info { Title = swaggerApiName, Version = apiVersion, Description = swaggerDescription });
                c.IncludeXmlComments(AppContext.BaseDirectory + @"SP.Services.API.xml");
                c.OperationFilter<FormFileOperationFilter>();
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                //c.OperationFilter<AddHeaderOperationFilter>("ClientId", "Client id sending this request. This authenticates the client and authorizes it for different access levels");
                //c.OperationFilter<AddHeaderOperationFilter>("ClientSecret", "Client Secret for client sending this request. This authenticates the client and authorizes it for different access levels");
            });

            //services.AddEntityFrameworkNpgsql()
            //    .AddDbContext<ServiceContext>(builder => builder.UseNpgsql(connectionString, opt => opt.MigrationsAssembly(securityMigrationsAssembly)).EnableSensitiveDataLogging(), ServiceLifetime.Scoped);

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<ServiceContext>(builder => builder.UseNpgsql(connectionString, opt => opt.MigrationsAssembly(securityMigrationsAssembly)), ServiceLifetime.Scoped);

            services.AddScoped<IEmail, Email>();
            services.AddScoped<IApplicationSettings, ApplicationSettings>();
            services.AddScoped<IProviderRepository, ProviderRepository>();
            services.AddScoped<IServiceRepository, ServiceRepository>();
            services.AddScoped<IHelperRepository, HelperRepository>();
            services.AddScoped<ISearchRepository, SearchRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IBillingAccountRepository, BillingAccountRepository>();
            services.AddScoped<IZoho, Zoho>();
            services.AddSingleton<IJwtFactory, JwtFactory>();

            // Register this to get access to the current user
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Register this to get access to the scope authorization handler
            services.AddSingleton<IAuthorizationHandler, HasScopeHandler>();

            // This is the Authorization policy provider that will resolve policies dynamically. It allows working with policies without explicitly specifying each one below
            //services.AddSingleton<IAuthorizationPolicyProvider, AuthorizationPolicyProvider>();



            /* /****** JWT WIRE UP ****** /*/

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            services.Configure<JwtIssuerOptions>(opt =>
            {
                opt.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                opt.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];

                long.TryParse(jwtAppSettingOptions[nameof(JwtIssuerOptions.ValidFor)], out long validForMinutes);
                opt.ValidFor = TimeSpan.FromMinutes(validForMinutes);
                opt.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(config =>
            {
                config.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                config.TokenValidationParameters = tokenValidationParameters;
                config.SaveToken = true;
                //config.Events = new JwtBearerEvents
                //{
                //    OnTokenValidated = async ctx =>
                //   {
                //       //string clientId = ctx.Principal.FindFirst("client_id");
                //       ctx.HttpContext.Request.HttpContext.Items.Add("clientId", clientId);
                //   }
                //};
            });

            services.AddAuthorization(opt =>
            {
                opt.AddPolicy(Constants.Strings.JwtPolicies.ApiAccess, policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.ApiAccess);
                });
                opt.AddPolicy(Constants.Strings.JwtPolicies.AdminAccess, policy => policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.AdminAccess));
                opt.AddPolicy(Constants.Strings.JwtPolicies.ClientAccess, policy => policy.RequireClaim(Constants.Strings.JwtClaimIdentifiers.Rol, Constants.Strings.JwtClaims.ClientAccess));
            });

            /****** End Jwt Wire Up *******/
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();


            app.UseAuthentication();

            //var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));


            //app.UseJwtBearerAuthentication(new JwtBearerOptions
            //{
            //    AutomaticAuthenticate = true,
            //    AutomaticChallenge = true,
            //    TokenValidationParameters = tokenValidationParameters
            //});


            ExceptionlessClient client = new ExceptionlessClient(c => { c.ApiKey = "aebKb62eArQGYAILSakAZpSOZjCw6JdpbZKOX5rF"; c.Enabled = true; c.ServerUrl = "http://mkng-prod-app01.westeurope.cloudapp.azure.com:84"; });
            app.UseExceptionless(client);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();

                //using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                //{
                //    var context = scope.ServiceProvider.GetService<ServiceContext>();

                //    context.Database.Migrate();
                //    context.EnsureSeed();
                //}
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }


            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());


            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationSeconds = 60 * 60 * 24 * 8;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = $"public,max-age={durationSeconds}";
                },
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"albums")),
                RequestPath = new PathString("/albums")
            });


            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"albums")),
                RequestPath = new PathString("/albums")
            });


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{swaggerApiName} {apiVersion}");
            });


            //app.UseIdentity();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            //Third party authentication

            app.UseMvc();
            /*app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            */
        }
    }
}