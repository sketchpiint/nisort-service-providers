﻿using System;
using System.Threading.Tasks;
using Exceptionless;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SP.Services.API.Globals;
using SP.Services.Core.Management.Logging;

namespace SP.Services.API.Filters
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(CustomException))
            {
                CustomException cex = context.Exception as CustomException;

                var result = new ObjectResult(cex.FriendlyMessage);
                result.StatusCode = (int)cex.HttpStatusCode;

                context.Result = result;
            }
            else
            {
                var result = new ObjectResult("An internal error occurred and your request could not be completed");
                result.StatusCode = 500;
                context.Result = result;
            }

            context.Exception.ToExceptionless(client: Global.ExeptionlessClient).SetHttpContext(context.HttpContext).Submit();

            context.ExceptionHandled = true;
            return base.OnExceptionAsync(context);
        }
    }


}
