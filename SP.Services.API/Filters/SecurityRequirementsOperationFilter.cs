﻿using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;

namespace SP.Services.API.Filters
{
    public class SecurityRequirementsOperationFilter : IOperationFilter
    {
        string _headerName;
        string _description;

        /// <summary>
        /// Initializes the Security Requirements Operation Filter for swagger UI
        /// </summary>
        /// <param name="headerName">Name of the authorization header</param>
        /// <param name="description">Describes the field to the API user</param>
        public SecurityRequirementsOperationFilter(string headerName = "Authorization", string description = "Bearer token for authentication and authorization of this reques. E.g Bearer eyJhbGciOiJI...")
        {
            _headerName = headerName;
            _description = description;
        }

        public void Apply(Operation operation, OperationFilterContext context)
        {
            if(context.ApiDescription.ActionAttributes().OfType<AllowAnonymousAttribute>().Any()) return;

            // Policy names map to scopes
            var controllerScopes = context.ApiDescription.ControllerAttributes()
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.Policy);

            var actionScopes = context.ApiDescription.ActionAttributes()
                .OfType<AuthorizeAttribute>()
                .Select(attr => attr.Policy);            

            var requiredScopes = controllerScopes.Union(actionScopes).Distinct();

            if (requiredScopes.Any())
            {
                operation.Responses.Add("401", new Response { Description = "Unauthorized" });
                operation.Responses.Add("403", new Response { Description = "Forbidden" });

                //operation.Security = new List<IDictionary<string, IEnumerable<string>>>();
                //operation.Security.Add(new Dictionary<string, IEnumerable<string>>
                //{
                //    { "oauth2", requiredScopes }
                //});                

                if (operation.Parameters == null)
                {
                    operation.Parameters = new List<IParameter>();
                }

                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = _headerName,
                    In = "header",
                    Description = _description,
                    Required = true,
                    Type = "string",
                    Default = "Bearer "
                });
            }
        }
    }
}
