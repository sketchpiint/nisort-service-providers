﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SP.Services.Automation.Core
{
    public static class SessionConfigs
    {
        public static string ApiBaseUrl { get; set; }
        public static string PageSize { get; set; }
    }
}
