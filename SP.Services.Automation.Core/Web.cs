﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SP.Services.Automation.Core
{
    public class Web
    {
        public static async Task<string> GetDataAsync(string queryUrl, string username, string password)
        {
            try
            {
                HttpClient client = new HttpClient();

                if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));

                string result = await client.GetStringAsync(queryUrl);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<string> PostDataAsync(string url, string data, string username = "", string password = "", bool auth = false)
        {
            var client = new HttpClient();

            if (auth) client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));

            var response = await client.PostAsync(url, new StringContent(data));

            //response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }
    }
}
