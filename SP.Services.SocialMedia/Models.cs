﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using SP.Services.SocialMedia.Entities;
using SP.Services.SocialMedia.Extensions;

namespace SP.Services.SocialMedia
{
    public class Models
    {
        /// <summary>
        /// Get's Model profiles to post to social media today
        /// </summary>
        internal async Task<List<Provider>> GetProfiles(int count)
        {
            string token = await GetAccessToken();
            //Properties.Settings.Default.ModelPostsCount
            string providersJson = await Web.GetDataAsync($"{Properties.Settings.Default.NdApiBaseUrl}Provider/GetWithSafeImages?providerCount={count}", new AuthInfo(token));

            List<dynamic> providersGeneral = JsonConvert.DeserializeObject<List<dynamic>>(providersJson);

            List<Provider> providers = new List<Provider>();

            providersGeneral.ForEach(pg =>
            {
                Provider provider = new Provider
                {
                    ProviderId = pg.providerId,
                    Age = pg.age,
                    Area = pg.area,
                    Location = pg.generalLocation,
                    Msisdn = pg.user.msisdn,
                    Name = pg.user.firstName,
                    Nationality = pg.nationality,
                    Username = pg.user.username
                };
                var userImage = GetImage((long)pg.userId).Result;
                provider.ImagesLocation = userImage.Image;
                provider.Thumbnail = userImage.Thumbnail;
                providers.Add(provider);
            });

            return providers;
        }

        internal async Task<(string Image, string Thumbnail)> GetImage(long userId)
        {
            string imagePath = $"{Properties.Settings.Default.ImagePath}{userId}";
            string token = await GetAccessToken();

            string imagesJson = await Web.GetDataAsync($"{ Properties.Settings.Default.NdApiBaseUrl}User/GetImages/{userId}?safeOnly=true", new AuthInfo(token));

            List<string> images = JsonConvert.DeserializeObject<List<string>>(imagesJson);

            Random rnd = new Random(0);

            string image = images[rnd.Next(images.Count)];

            return ($"{imagePath}/{image}", $"{imagePath}/Thumbnails/{image}");
        }

        internal async Task<string> GetAccessToken()
        {
            if (Settings.TimeFetched != null && !Settings.AccessToken.IsNull()) return Settings.AccessToken;

            Settings.TimeFetched = DateTime.Now;
            string tokenJson = await Web.PostDataAsync($"{Properties.Settings.Default.NdApiBaseUrl}Auth/GetAccessToken", JsonConvert.SerializeObject(
                new
                {
                    clientId = "SP.Services.web",
                    clientSecret = "a7322ee4-2733-4181-a7e3-8fadc8495973"
                }));

            dynamic result = JsonConvert.DeserializeObject<ExpandoObject>(tokenJson, new ExpandoObjectConverter());
            dynamic token = JsonConvert.DeserializeObject<ExpandoObject>(result.token, new ExpandoObjectConverter());

            //var result1 = JsonConvert.DeserializeObject(tokenJson);

            Settings.AccessToken = token.auth_token;
            Settings.Validity = (int)token.expires_in;

            return token.auth_token;
        }
    }
}
