﻿using Exceptionless.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using SP.Services.SocialMedia.Entities;
using System.Net;
using System.Globalization;
using System.Dynamic;
using Exceptionless.Json.Converters;

namespace SP.Services.SocialMedia
{
    public class Buffer
    {
        public Buffer()
        {
            GetProfiles().Wait();
        }
        public string AccessKey { get; private set; } = Properties.Settings.Default.BufferAccessKey;
        public string BaseUrl { get; private set; } = Properties.Settings.Default.BufferBaseUrl;
        public bool ShortenLinks { get; private set; } = Properties.Settings.Default.BufferShortenLink;
        public List<Profile> Profiles { get; set; } = new List<Profile>();

        public async Task<dynamic> GetProfiles()
        {
            string profilesJson = await Web.GetDataAsync($"{BaseUrl}profiles.json?access_token={AccessKey}");

            List<dynamic> profiles = JsonConvert.DeserializeObject<List<dynamic>>(profilesJson);

            Profiles = new List<Profile>();

            profiles.ForEach(p =>
            {
                Profiles.Add(new Profile
                {
                    Id = p.id,
                    Username = p.service_username,
                    Service = p.service,
                    ServiceId = p.service_id,
                    FormattedUsername = p.formatted_username
                });
            });

            return profiles;
        }

        /// <summary>
        /// Queues a post without an image
        /// </summary>
        /// <param name="postText"></param>
        /// <param name="scheduledAtUtc"></param>
        /// <param name="mediaUrl"></param>
        /// <param name="mediaTitle"></param>
        /// <param name="mediaDescription"></param>
        /// <returns></returns>
        public async Task QueuePost(string postText, DateTime scheduledAtUtc, string mediaUrl = "", string mediaTitle = "", string mediaDescription = "")
        {
            string profileIds = "";

            Profiles.ToList().ForEach(p =>
            {
                if (p.ServiceId == "6986188912") return; 

                profileIds += $"profile_ids[]={p.Id}&";
            });

            string responseJson = await Web.PostFormDataAsync($"{BaseUrl}updates/create.json?access_token={AccessKey}", $"{profileIds}text={postText}&media[link]={WebUtility.UrlEncode(mediaUrl)}&media[description]={WebUtility.UrlEncode(mediaDescription)}&scheduled_at={scheduledAtUtc.ToString("o", CultureInfo.InvariantCulture)}");

            dynamic response = JsonConvert.DeserializeObject<ExpandoObject>(responseJson, new ExpandoObjectConverter());
        }

        /// <summary>
        /// Queues a post with an image
        /// </summary>
        /// <param name="postText"></param>
        /// <param name="scheduledAtUtc"></param>
        /// <param name="mediaUrl"></param>
        /// <param name="mediaThumbnail"></param>
        /// <returns></returns>
        public async Task QueuePost(string postText, DateTime scheduledAtUtc, string mediaUrl = "", string mediaThumbnail = "")
        {
            mediaUrl = mediaUrl.Replace(" ", "%20");
            mediaThumbnail = mediaThumbnail.Replace(" ", "%20");
            //postText = WebUtility.UrlEncode(postText).Replace("+", "%20");

            string profileIds = "";

            Profiles.Select(p => p.Id).ToList().ForEach(p => profileIds += $"profile_ids[]={p}&");

            string responseJson = await Web.PostFormDataAsync($"{BaseUrl}updates/create.json?access_token={AccessKey}", $"{profileIds}text={postText}&media[photo]={WebUtility.UrlEncode(mediaUrl)}&media[thumbnail]={WebUtility.UrlEncode(mediaThumbnail)}&scheduled_at={scheduledAtUtc.ToString("o", CultureInfo.InvariantCulture)}");

            dynamic response = JsonConvert.DeserializeObject<ExpandoObject>(responseJson, new ExpandoObjectConverter());

        }
    }
}
