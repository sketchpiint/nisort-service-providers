﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Exceptionless;
using System.Timers;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace SP.Services.SocialMedia
{
    public partial class MainHandler : ServiceBase
    {
        Timer timer;
        ExceptionlessClient _exceptionlessClient;

        public MainHandler()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            IntializeExceptionless();

        }

        protected override void OnStop()
        {
        }

        private void IntializeExceptionless()
        {
            _exceptionlessClient = new ExceptionlessClient(c =>
            {
                c.ApiKey = Properties.Settings.Default.ExceptionlessKey;
                c.Enabled = true;
                c.ServerUrl = Properties.Settings.Default.ExceptionlessUrl;
            });

            _exceptionlessClient.Startup();
        }
    }
}
