﻿namespace SP.Services.SocialMedia.Entities
{
    internal class Provider
    {
        public long ProviderId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Msisdn { get; set; }
        public string ImagesLocation { get; set; }
        public string Location { get; set; }
        public string Area { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public string Thumbnail { get; set; }
    }
}
