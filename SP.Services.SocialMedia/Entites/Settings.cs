﻿using System;

namespace SP.Services.SocialMedia.Entities
{
    internal static class Settings
    {
        internal static string AccessToken = "";
        internal static int Validity = 0;
        internal static DateTime? TimeFetched = null;
    }
}
