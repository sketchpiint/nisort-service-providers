﻿namespace SP.Services.SocialMedia.Entities
{
    public class Profile
    {
        public string Id { get; set; }
        public string ServiceId { get; set; }
        public string Username { get; set; }
        public string Service { get; set; }
        public string FormattedUsername { get; set; }
    }
}
