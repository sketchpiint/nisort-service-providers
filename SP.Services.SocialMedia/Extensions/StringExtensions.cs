﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP.Services.SocialMedia.Extensions
{
    public static class StringExtensions
    {
        public static (int feet, int inches) SplitFeetInches(this string feetInches)
        {
            if (String.IsNullOrEmpty(feetInches)) return (0, 0);

            string[] res = feetInches.Split('\'');

            return (Int32.Parse(res[0]), res.Length > 1 ? Int32.Parse(res[1]) : 0);
        }

        /// <summary>
        /// Checks if a string is null or empty
        /// </summary>
        /// <param name="_string"></param>
        /// <param name="trueIfZero">If the provided string is a 0, i.e "0", assumes the string to be null. 
        /// Mostly usefull in specific situations</param>
        /// <returns>True if the string evaluates to null</returns>
        public static bool IsNull(this string _string, bool trueIfZero = false)
        {
            if (trueIfZero) return String.IsNullOrEmpty(_string) || _string.Trim() == "0";
            return String.IsNullOrEmpty(_string);
        }

        /// <summary>
        /// Returns the same (old) string if the <para>newValue</para> is null or empty. Provides '??' functionality for strings
        /// </summary>
        /// <param name="_string">String whose value is changed - returned if newValue is null</param>
        /// <param name="newValue">New string whose value is checked for null or empty</param>
        /// <returns></returns>
        public static string Coalesce(this string _string, string newValue) => String.IsNullOrEmpty(newValue) ? _string : newValue;
    }
}
