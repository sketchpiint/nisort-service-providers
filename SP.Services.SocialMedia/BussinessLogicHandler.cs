﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SP.Services.SocialMedia.Entities;

namespace SP.Services.SocialMedia
{
    public class BussinessLogicHandler
    {
        public async Task QueueDailyQuotePosts()
        {
            RandomQuotes quotes = new RandomQuotes();
            Twitter twitter = new Twitter();
            Buffer buffer = new Buffer();

            List<string> quotesHours = Properties.Settings.Default.QuotesHours.Split(',').ToList();


            List<string> trends = await twitter.GetTrends();

            string trendsString = "";

            trends.ForEach(t => trendsString += $" {t}");

            int timeIndex = -1;
            quotesHours.ForEach((async qh =>
            {
                int.TryParse(quotesHours[++timeIndex], out int time);

                if (DateTime.Now.Hour > time) return;

                var quote = await quotes.GetQuote(quotesHours.Count);

                string quoteString = $"{quote.quote}\n - {quote.author} http://nairobidivas.com.\n{trendsString}";

                await buffer.QueuePost(quoteString, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, time, 0, 0, DateTimeKind.Utc), "http://wwww.nairobidivas.com", "Nairobi Divas", "Come check us out for the best divas in town");
            }));
        }

        public async Task QueueDailyModelPosts()
        {
            Models models = new Models();
            Twitter twitter = new Twitter();
            Buffer buffer = new Buffer();

            List<string> modelsHours = Properties.Settings.Default.ModelsHours.Split(',').ToList();
            int modelsCount = modelsHours.Count;

            List<Provider> providers = await models.GetProfiles(modelsCount);


            List<string> trends = await twitter.GetTrends();

            string trendsString = "";

            trends.ForEach(t => trendsString += $" {t}");

            int timeIndex = -1;
            providers.ForEach(async p =>
            {
                int.TryParse(modelsHours[++timeIndex], out int time);

                if (DateTime.Now.Hour > time) return;

                await buffer.QueuePost($"Shout out to our fans in {p.Area}. Sweet {p.Name} could use some of your company. Check her out at http://nairobidivas.com/escort/{p.Username}.\n{trendsString}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, time, 0, 0, DateTimeKind.Utc), p.ImagesLocation, p.Thumbnail);
            });
        }

    }
}
