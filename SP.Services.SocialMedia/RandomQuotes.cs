﻿using Newtonsoft.Json;
using System.Threading.Tasks;

namespace SP.Services.SocialMedia
{
    internal class RandomQuotes
    {
        public string BaseUrl { get; set; } = Properties.Settings.Default.RandomQuotesApiUrl;
        public string Key { get; set; } = Properties.Settings.Default.RQKey;
        public string Category { get; set; } = Properties.Settings.Default.RQCategory;

        public async Task<(string quote, string author)> GetQuote(int count)
        {
            string resultJson = await Web.GetDataAsync($"{BaseUrl}?cat={Category}&count={count}", new AuthInfo(Key, "X-Mashape-Key") { Accept = "application/json" });

            dynamic result = JsonConvert.DeserializeObject(resultJson);

            return (result.quote, result.author);
        }
    }
}
