﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using SP.Services.SocialMedia.Extensions;

namespace SP.Services.SocialMedia
{
    internal class Web
    {
        internal static async Task<string> GetDataAsync(string queryUrl, AuthInfo authType = null)
        {
            try
            {
                HttpClient client = new HttpClient();

                if (authType != null) client = authType.GetAuthHeader();

                string result = ""; // await client.GetStringAsync(queryUrl);

                List<Task> tasks = new List<Task>();
                tasks.Add(Task.Run(async () => result = await client.GetStringAsync(queryUrl)));

                Task.WaitAll(tasks.ToArray());


                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static async Task<string> PostDataAsync(string url, string data, AuthInfo authType = null, string mediaType = "application/json")
        {
            var client = new HttpClient();

            if (authType != null) client = authType.GetAuthHeader();

            var response = await client.PostAsync(url, new StringContent(data, Encoding.UTF8, mediaType));

            //response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        internal static async Task<string> PostFormDataAsync(string url, string data, AuthInfo authType = null, string mediaType = "application/x-www-form-urlencoded")
        {
            var client = new HttpClient();

            if (authType != null) client = authType.GetAuthHeader();

            HttpResponseMessage response = new HttpResponseMessage();

            List<Task> tasks = new List<Task>();
            tasks.Add(Task.Run (async () => response = await client.PostAsync(url, new StringContent(data, Encoding.UTF8, mediaType))));

            Task.WaitAll(tasks.ToArray());

            //response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }
    }

    internal enum AuthType
    {
        Basic = 1,
        Bearer,
        CustomHeader
    }

    internal class AuthInfo
    {
        /// <summary>
        /// Initializes a Basic authentication type with the provided credentials
        /// </summary>
        /// <param name="username">Username to use in the authorization header</param>
        /// <param name="password">Password to use in the authorization header</param>
        /// <param name="authType">The authorization scheme to use. This defaults to the Basic scheme</param>
        public AuthInfo(string username, string password, AuthType authType = AuthType.Basic)
        {
            Username = username;
            Password = password;
            AuthType = authType;

            if (authType == AuthType.Bearer) throw new InvalidOperationException($"A bearer authentication requires a token and not {nameof(username)} and {nameof(password)}");
        }

        /// <summary>
        /// Initializes a Bearer authentication type with the provided token
        /// </summary>
        /// <param name="jwtToken">The token to use in the authorization header</param>
        public AuthInfo(string jwtToken)
        {
            JwtToken = jwtToken;
            AuthType = AuthType.Bearer;
        }

        /// <summary>
        /// Initiates authentication with a custom header
        /// </summary>
        /// <param name="key">The api key</param>
        /// <param name="customHeader">Name of the custom header to place the key</param>
        public AuthInfo(string key, string customHeader)
        {
            Key = key;
            HeaderName = customHeader;
            AuthType = AuthType.CustomHeader;
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string JwtToken { get; set; }
        public AuthType AuthType { get; set; } = AuthType.Basic;
        public string Key { get; set; }
        public string HeaderName { get; set; }
        public string Accept { get; set; } = "";

        public HttpClient GetAuthHeader(HttpClient client = null)
        {
            client = client ?? new HttpClient();

            if (!Accept.IsNull()) client.DefaultRequestHeaders.Add(nameof(Accept), Accept);

            switch (AuthType)
            {
                case AuthType.Basic:
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes($"{Username}:{Password}")));
                    return client;
                case AuthType.Bearer:
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", JwtToken);
                    return client;
                case AuthType.CustomHeader:
                    client.DefaultRequestHeaders.Add(HeaderName, Key);                    
                    return client;
                default:
                    throw new ArgumentOutOfRangeException("Only Basic and Bearer authentication schemes provided for");
            }
        }
    }
}
