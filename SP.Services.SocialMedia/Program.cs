﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SP.Services.SocialMedia
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            Buffer buffer = new Buffer();

            //new BussinessLogicHandler().GetTrends().Wait();
            //new BussinessLogicHandler().GetProfiles().Wait();
            new BussinessLogicHandler().QueueDailyModelPosts().Wait();
            new BussinessLogicHandler().QueueDailyQuotePosts().Wait();
            //buffer.QueuePost(
            //    "Shout out to our fans in Nairobi cbd. Sweet Bella could use some of your company. Check her out at http://nairobidivas.com/escort/bella1",
            //     new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0).ToUniversalTime(),
            //     "http://media.nairobidivas.com/albums/421/Bella_Nairobi Divas_010218120414_421.jpg", 
            //    "http://media.nairobidivas.com/albums/421/Thumbnails/Bella_Nairobi Divas_010218120414_421.jpg").Wait();
            //buffer.GetProfiles().Wait();

#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MainHandler()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
